#include <Arduino.h>

#include "ADS1X15.h"
#include "SSD1306Wire.h"

#define SAMPLES 6
#define DELAY_SAMPLE 5
#define DELAY_MEASURE 200

//I2C - 5 SDA, 6 SCL
//ADS1115 analog to digital converter i2c connected
//Any thin SSD1306 oled i2c connected

// Heating capsule at heatPin
// MOSFET, looking at the front of the chip (the side with no metal plate): left to right - gate - drain - source
// Using a 24v 50w capsule
// heatPin -- 1k ohm --> IRLB3034 (MOSFET) GATE
// IRLB3034 (MOSFET) GATE -- 10k ohm --> common ground (must be common between esp and power supply!)
// IRLB3034 (MOSFET) DRAIN --> heating capsule (-)
// IRLB3034 (MOSFET) SOURCE --> common ground, power supply -
// heating capsule (+) --> power supply +

// https://en.wikipedia.org/wiki/Thermistor

const float R = 100000;  // Fixed resistor value (100k ohms)
const float R0 = 100000;  // Resistance of thermistor at 25°C
const float T0 = 298.15;  // Reference temperature in Kelvin (25°C)
const float B = 3950;  // Beta coefficient

ADS1115 ADS(0x48);
SSD1306Wire display(0x3c, 0, 1, GEOMETRY_128_32); 

int heatPin = 1;

float temperature_measurement;
int current_sample = 0;

bool heating;

float KtoC(float k)
{
  return k - 273.15f;
}

void SerialPrintTempK(float T)
{
  Serial.print("Temperature: ");
  Serial.print(T);
  Serial.print("K ");

  Serial.print(KtoC(T));
  Serial.println("C");
}

void setup()
{
  //Serial.begin(115200);

  pinMode(heatPin, OUTPUT); 
  analogWrite(heatPin, 0);

  Wire.begin(5, 6);

  ADS.begin();
  ADS.setGain(0);

  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);

  Serial.println("Ready!");
}

void DisplayTemp(float TK)
{
  display.clear();

  display.setFont(ArialMT_Plain_16);
  display.drawString(0, 0, "T: "+String(KtoC(TK))+"C");

  if(heating)
  {
    display.drawString(0, 17, "HEATING");
  }
  else
  {
    display.drawString(0, 17, "IDLE");
  }

  display.display();
}

void loop() 
{
  int16_t adc0 = ADS.readADC(0);
  float f = ADS.toVoltage(1);
  float volts = adc0 * f;

  float Rthermistor = R * (3.3 / volts - 1); 
  float T = 1.0 / (1.0 / T0 + 1.0 / B * log(Rthermistor / R0));

  temperature_measurement += T;
  current_sample++;

  if(current_sample >= SAMPLES)
  {
    float avg_t = temperature_measurement/SAMPLES;
    SerialPrintTempK(avg_t);
    DisplayTemp(avg_t);

    if(KtoC(avg_t) < 45)
    {
      analogWrite(heatPin, 10);
      heating = true;
    }
    else
    {
      analogWrite(heatPin, 0);
      heating = false;
    }

    current_sample = 0;
    temperature_measurement = 0;
    delay(DELAY_MEASURE);
  }
  else
  {
    delay(DELAY_SAMPLE);
  }
}
