import argparse
import math

def calculate_velocity(rpm, diameter):
    # Convert diameter from millimeters to meters
    diameter_meters = diameter / 1000

    # Calculate circumference of the wheel
    circumference = math.pi * diameter_meters

    # Calculate velocity in meters per second
    velocity_mps = (rpm * circumference) / 60

    # Convert velocity to kilometers per hour
    velocity_kph = velocity_mps * 3.6

    # Convert velocity to centimeters per second
    velocity_cms = velocity_mps * 100

    return velocity_mps, velocity_kph, velocity_cms

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Calculate velocity based on RPM and diameter.")
    parser.add_argument("-r", "--rpm", type=int, help="Rotations per minute")
    parser.add_argument("-d", "--diameter", type=float, help="Diameter of the wheel in millimeters")
    args = parser.parse_args()

    if args.rpm is None or args.diameter is None:
        print("Please provide both RPM and diameter.")
    else:
        velocity_mps, velocity_kph, velocity_cms = calculate_velocity(args.rpm, args.diameter)
        print("Estimated velocity: {:.2f} m/s, {:.2f} km/h, {:.2f} cm/s".format(velocity_mps, velocity_kph, velocity_cms))
