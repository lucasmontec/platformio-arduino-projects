#include <Arduino.h>

#include "Commands/ICommandExecutor.h"
#include "Bluetooth/IControlCommandExecutor.h"

#pragma once

class PrintCommandExecutor : public ICommandExecutor {
    void execute(const String& command) override {
        Serial.println("Config command received: "+command);
    }
};

class PrintControlCommandExecutor : public IControlCommandExecutor {
    void execute(const String& command) override {
        Serial.println("Control command received: "+command);
    }
    void standby() override {
        Serial.println("Standby");
    }
};