#include <Arduino.h>
#include <Wire.h> 
#include <VL6180X.h>

#include "Bluetooth/BluetoothController.h"
#include "Movement/InputStateMotorCommandExecutor.h"
#include "Movement/MotorController.h"
#include "Commands/CommandExecutor.h"
#include "Commands/Commands.h"

#include "Sensors/Battery.h"
#include "Sensors/LaserRangeController.h"

#include "Status.h"

#include "Leds/LedController.h"
#include "Leds/LedEffects.h"
#include "Leds/LedCommands.h"

//#include "OTAController.h" //Too big for esp32c3

#define LED_PIN 4

BluetoothCommandController* btCommandController;
InputStateMotorCommandExecutor* motorControlCommandExecutor;

MotorController motorController(21, 20, 10, 9);
CommandExecutor executor = CommandExecutor(commands, commandCount);

LedController ledController = LedController(2, LED_PIN, NEO_GRB + NEO_KHZ800);

PlayEffectCommand* ledEffectsCommand = new PlayEffectCommand(&ledController);

BatteryMonitor* batteryMonitor = nullptr;

LaserRangeController& laserRangeController = LaserRangeController::getInstance();

//I2C
int sdaPin = 6;
int sclPin = 5;
long i2cFreq = 250000;

void setup() 
{
  Serial.begin(115200);
  //while (!Serial) delay(10);

  Wire.begin(sdaPin, sclPin, i2cFreq);
  delay(10);

  batteryMonitor = BatteryMonitor::getInstance();
  batteryMonitor->Init();
  
  laserRangeController.Init();

  motorController.SetMotorBCalibration(200); //Calibration
  motorController.init(true);

  motorControlCommandExecutor = new InputStateMotorCommandExecutor(&motorController);
  btCommandController = new BluetoothCommandController(motorControlCommandExecutor, &executor);

  motorControlCommandExecutor->SetTurningSpeedMultiplier(0.58f);

  btCommandController->Init();

  ledController.Init();

  if(batteryMonitor != nullptr && batteryMonitor->initializationFault())
  {
    ledController.EnqueueEffect(new BlinkEffect(Adafruit_NeoPixel::Color(255,0,0), 0, 200, 4));
    ledController.EnqueueEffect(new SetColorEffect(0));
    globalStatus = "Battery monitor fault!";
  }
  else
  {
    ledController.EnqueueEffect(new BlinkEffect(Adafruit_NeoPixel::Color(0,0,255), 0, 200, 2));
    ledController.EnqueueEffect(new BlinkEffect(Adafruit_NeoPixel::Color(0,255,0), 0, 200, 2));
    ledController.EnqueueEffect(new SetColorEffect(0));
    globalStatus = "CatBot Ready!";
  }

  ledController.SetBrightness(70);
  
  executor.addCommand(ledEffectsCommand);
}

void loop() 
{
  btCommandController->UpdateControlService();
  ledController.Update();

  laserRangeController.Update();
}
