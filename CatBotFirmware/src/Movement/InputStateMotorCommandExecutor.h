#include <Arduino.h>
#include "Bluetooth/IControlCommandExecutor.h"
#include "MotorController.h"

#pragma once

class InputStateMotorCommandExecutor : public IControlCommandExecutor
{
private:
    MotorController *motorController;
    float turningSpeedMultiplier = 1.0f;

public:
    explicit InputStateMotorCommandExecutor(MotorController *motorController) : motorController(motorController) {}

    void SetTurningSpeedMultiplier(float multiplier)
    {
        turningSpeedMultiplier = multiplier;
    }

    void Execute(const String &command) override
    {
        if (command == "stb")
        {
            Standby();
            return;
        }

        // Unified stop command
        if (command == "stpa")
        {
            motorController->BrakeA();
            return;
        }

        if (command == "stpb")
        {
            motorController->BrakeB();
            return;
        }

        if (command == "stp")
        {
            motorController->Brake();
            return;
        }

        // Handle tank mode: <motora_value>:<motorb_value>
        int colonIndex = command.indexOf(':');
        if (colonIndex != -1)
        {
            float motorASpeed = command.substring(colonIndex + 1).toFloat() / 1024.0f;
            float motorBSpeed = command.substring(0, colonIndex).toFloat() / 1024.0f;

            motorController->SetMotorA(motorASpeed);
            motorController->SetMotorB(motorBSpeed);
            return;
        }

        bool forward = command.indexOf('f') != -1;
        bool backward = command.indexOf('b') != -1;
        bool right = command.indexOf('r') != -1;
        bool left = command.indexOf('l') != -1;

        if (forward && !backward)
        {
            motorController->Forward();
        }
        else if (backward && !forward)
        {
            motorController->Backward();
        }

        if (right && !left)
        {
            motorController->BackwardA(turningSpeedMultiplier);
            motorController->ForwardB(turningSpeedMultiplier);
        }
        else if (left && !right)
        {
            motorController->ForwardA(turningSpeedMultiplier);
            motorController->BackwardB(turningSpeedMultiplier);
        }
    }

    void Standby() override
    {
        motorController->Standby();
    }
};
