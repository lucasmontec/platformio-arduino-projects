#include "MotorController.h"

MotorController::MotorController(uint8_t in1, uint8_t in2, uint8_t in3, uint8_t in4, uint8_t speedA, uint8_t speedB)
    : motorA(in1, in2), motorB(in3, in4), calibrationA(speedA), calibrationB(speedB) 
{}

void MotorController::init(bool invertA, bool invertB)
{
    motorA.init(invertA);
    motorB.init(invertB);
}

void MotorController::SetMotorACalibration(uint8_t cal) 
{
    calibrationA = cal;
}

void MotorController::SetMotorBCalibration(uint8_t cal) 
{
    calibrationB = cal;
}

void MotorController::SetMotorA(float value) 
{
    uint8_t speed = static_cast<uint8_t>(abs(value) * 255);
    if (value >= 0) 
    {
        motorA.forward(speed);
    }
    else
    {
        motorA.backward(speed);
    }
}

void MotorController::SetMotorB(float value) 
{
    uint8_t speed = static_cast<uint8_t>(abs(value) * 255);
    if (value >= 0) 
    {
        motorB.forward(speed);
    } 
    else 
    {
        motorB.backward(speed);
    }
}

void MotorController::ForwardA(float multiplier) 
{
    motorA.forward(static_cast<uint8_t>(calibrationA * multiplier));
}

void MotorController::BackwardA(float multiplier) 
{
    motorA.backward(static_cast<uint8_t>(calibrationA * multiplier));
}

void MotorController::ForwardB(float multiplier) 
{
    motorB.forward(static_cast<uint8_t>(calibrationB * multiplier));
}

void MotorController::BackwardB(float multiplier) 
{
    motorB.backward(static_cast<uint8_t>(calibrationB * multiplier));
}

void MotorController::BrakeA() 
{
    motorA.brake();
}

void MotorController::BrakeB() 
{
    motorB.brake();
}

void MotorController::Forward() 
{
    motorA.forward(calibrationA);
    motorB.forward(calibrationB);
}

void MotorController::Backward() 
{
    motorA.backward(calibrationA);
    motorB.backward(calibrationB);
}

void MotorController::Standby() 
{
    motorA.standby();
    motorB.standby();
}

void MotorController::Brake() 
{
    motorA.brake();
    motorB.brake();
}
