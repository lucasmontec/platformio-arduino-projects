#pragma once

#include <Arduino.h>

class Motor {
public:
    Motor(uint8_t forwardPin, uint8_t backwardPin);

    void init(bool inverted = false);
    void forward(uint8_t speed = 255);
    void backward(uint8_t speed = 255);
    void brake();
    void standby();

private:
    uint8_t forwardPin, backwardPin;
    bool inverted = false;
};
