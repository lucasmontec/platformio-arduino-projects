#include <Arduino.h>

#include "Bluetooth/IControlCommandExecutor.h"
#include "MotorController.h"

#pragma once

class MotorControlCommandExecutor : public IControlCommandExecutor 
{
private:
    MotorController* motorController;
    float turningSpeedMultiplier = 1.0f;

public:
    explicit MotorControlCommandExecutor(MotorController* motorController) : motorController(motorController) {}

    void SetTurningSpeedMultiplier(float multiplier)
    {
        turningSpeedMultiplier = multiplier;
    }

    void Execute(const String& command) override 
    {
        if (command == "f") 
        {
            motorController->Forward();
            return;
        } 

        if (command == "b") 
        {
            motorController->Backward();
            return;
        } 

        if (command == "r") 
        {
            motorController->BackwardA(turningSpeedMultiplier);
            motorController->ForwardB(turningSpeedMultiplier);
            return;
        } 

        if (command == "l") 
        {
            motorController->ForwardA(turningSpeedMultiplier);
            motorController->BackwardB(turningSpeedMultiplier);
            return;
        } 

        if (command == "br") 
        {
            motorController->Brake();
            return;
        } 

        if (command == "stb") 
        {
            motorController->Standby();
            return;
        }

        if (command.startsWith("ma ")) 
        {
            float speed = command.substring(3).toInt() / 1024.0f;
            if (speed >= 0) {
                motorController->ForwardA(speed);
            } else {
                motorController->BackwardA(-speed);
            }
            return;
        } 

        if (command.startsWith("bra"))
        {
            motorController->BrakeA();
        }
        if (command.startsWith("brb")) 
        {
            motorController->BrakeB();
        }

        if (command.startsWith("mb ")) 
        {
            float speed = command.substring(3).toInt() / 1024.0f;
            if (speed >= 0) {
                motorController->ForwardB(speed);
            } else {
                motorController->BackwardB(-speed);
            }
            return;
        }
    }

    void Standby() override 
    {
        motorController->Standby();
    }
};