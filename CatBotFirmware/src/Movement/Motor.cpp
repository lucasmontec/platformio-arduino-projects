#include "Motor.h"

Motor::Motor(uint8_t forwardPin, uint8_t backwardPin) 
    : forwardPin(forwardPin), backwardPin(backwardPin) 
{}

void Motor::init(bool inverted) 
{
    pinMode(forwardPin, OUTPUT);
    pinMode(backwardPin, OUTPUT);
    this->inverted = inverted;
}

void Motor::forward(uint8_t speed) 
{
    analogWrite(forwardPin, inverted ? 0 : speed);
    analogWrite(backwardPin, inverted ? speed : 0);
}

void Motor::backward(uint8_t speed) 
{
    analogWrite(forwardPin, inverted ? speed : 0);
    analogWrite(backwardPin, inverted ? 0 : speed);
}

void Motor::brake() 
{
    analogWrite(forwardPin, 255);
    analogWrite(backwardPin, 255);
}

void Motor::standby() 
{
    analogWrite(forwardPin, 0);
    analogWrite(backwardPin, 0);
}
