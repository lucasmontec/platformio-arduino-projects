//FOR TC1508A

#pragma once
#include "Motor.h"

class MotorController {
public:
    MotorController(uint8_t in1, uint8_t in2, uint8_t in3, uint8_t in4, uint8_t speedA = 255, uint8_t speedB = 255);
    
    void init(bool invertA = false, bool invertB = false);
    void SetMotorACalibration(uint8_t cal);
    void SetMotorBCalibration(uint8_t cal);
    void SetMotorA(float value);
    void SetMotorB(float value);
    void ForwardA(float multiplier = 1.0f);
    void BackwardA(float multiplier = 1.0f);
    void ForwardB(float multiplier = 1.0f);
    void BackwardB(float multiplier = 1.0f);
    void BrakeA();
    void BrakeB();
    void Forward();
    void Backward();
    void Standby();
    void Brake();

private:
    uint8_t calibrationA;
    uint8_t calibrationB;
    Motor motorA;
    Motor motorB;
};
