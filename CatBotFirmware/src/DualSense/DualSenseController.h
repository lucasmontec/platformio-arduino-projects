#include <Arduino.h>

#pragma once

class DualSense
{
private:
    String mac;

public:
    DualSense(const char* mac) : mac(mac) {}
    void begin();
};