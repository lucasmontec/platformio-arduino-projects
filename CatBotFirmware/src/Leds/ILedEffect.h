#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#pragma once

class ILedEffect
{
public:
    void Setup(Adafruit_NeoPixel* strip);

    virtual void Resume() {}
    virtual void Update() {}
    virtual bool Ended() = 0;

    int NumPixels()
    {
        return strip->numPixels();
    }

protected:
    Adafruit_NeoPixel* strip;
    virtual void Start() {}

    void setAllLeds(uint32_t color = Adafruit_NeoPixel::Color(0, 0, 0));
    void setPixel(uint16_t pixel, uint32_t color);
};