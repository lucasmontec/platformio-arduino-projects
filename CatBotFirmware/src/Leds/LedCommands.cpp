#include "LedCommands.h"

/*
Commands for leds start with l
Pattern:
l c? <command> <command_parameter> <command_parameter>...

c means continuous
*/

PlayEffectCommand::PlayEffectCommand(LedController *ledController) : Command("l"), ledController(ledController) {}

uint32_t PlayEffectCommand::parseColor(const String &color)
{
    if(color.isEmpty())
    {
        Adafruit_NeoPixel::Color(200, 50, 245);
    }

    if (color == "r")
        return ColorRed();
    else if (color == "g")
        return ColorGreen();
    else if (color == "b")
        return ColorBlue();
    else if (color == "y")
        return ColorYellow();
    else if (color == "m")
        return ColorMagenta();
    else if (color == "c")
        return ColorCyan();
    else if (color == "w")
        return Adafruit_NeoPixel::Color(255, 255, 200);

    int red, green, blue;
    sscanf(color.c_str(), "%d-%d-%d", &red, &green, &blue);
    //Serial.println("Parsed custom color: "+String(red)+" "+String(green)+" "+String(blue));
    return Adafruit_NeoPixel::Color(red, green, blue);
}

// bl <color>:<on_off_time_ms?(default 500ms)>:<count?(default 2)>
// Examples:
// bl r:180:3 // Blinks red 3 times with 180ms duration
// bl r:300 // Blinks red 1 times (default) with 300ms duration
// bl r::3 // Blinks red 3 times with 500ms (default) duration
// bl 125-50-0:250:1 // Blinks RGB(125,50,0) once with 250 duration
// bl 255-0-0 // Blinks RGB(255,0,0) 1 times for default 500ms duration
void PlayEffectCommand::handleBlink(const String &params, bool setContinuous)
{
    uint32_t color = Adafruit_NeoPixel::Color(0, 0, 255);
    unsigned long onTime = 500, offTime = 500;
    int blinkCount = 1;

    int firstSeparator = params.indexOf(PARAM_SEPARATOR);
    if (firstSeparator != -1)
    {
        String colorStr = params.substring(0, firstSeparator);//r
        colorStr.trim();
        color = parseColor(colorStr);

        String extraParams = params.substring(firstSeparator + 1);//300 
        int secondSeparator = extraParams.indexOf(PARAM_SEPARATOR);
        if (secondSeparator != -1)
        {
            String durationStr = secondSeparator == 0 ? "" : extraParams.substring(0, secondSeparator); //Duration is the second and is optional
            String countStr = extraParams.substring(secondSeparator + 1);

            durationStr.trim();
            countStr.trim();
            onTime = offTime = durationStr.isEmpty() ? 500 : durationStr.toInt();
            blinkCount = countStr.isEmpty() ? 1 : countStr.toInt();
        }
        else
        {
            // No third parameter like this "bl r:300"
            extraParams.trim();
            onTime = offTime = extraParams.toInt();
        }
    }
    else
    {
        color = parseColor(params);
        blinkCount = 1; // Default to 1 if no additional params are given
    }

    if(setContinuous)
    {
        blinkCount = -1;
    }

    BlinkEffect *effect = new BlinkEffect(color, 0, onTime, offTime, blinkCount);
    if (setContinuous)
    {
        ledController->SetContinuousEffect(effect);
    }
    else
    {
        ledController->EnqueueEffect(effect);
    }
}

// rg <duration=1000>:<colorNear?>:<colorFar>?
// Examples:
// rg 1000:r:g // Range color for 1s from red (near) to green (far)
// rg 1000:r // Range color for 1s from red (near)
// rg 1000:: // Range color for 1s to yellow (far)
// rg 1000 // Range color for 1s
void PlayEffectCommand::handleRange(const String &params, bool setContinuous)
{
    int duration = 1000;
    uint32_t colorClose = Adafruit_NeoPixel::Color(255, 0, 0);
    uint32_t colorFar = Adafruit_NeoPixel::Color(0, 255, 0);

    int firstSeparator = params.indexOf(PARAM_SEPARATOR);
    if (firstSeparator != -1)
    {
        String durationStr = params.substring(0, firstSeparator);//1000
        durationStr.trim();
        if(!durationStr.isEmpty())
        {
            duration = durationStr.toInt();
        }
        else
        {
            duration = -1;
        }

        String extraParams = params.substring(firstSeparator + 1);//r:g or just r 
        int secondSeparator = extraParams.indexOf(PARAM_SEPARATOR);
        if (secondSeparator != -1) //r:g
        {
            String nearColorStr = secondSeparator == 0 ? "" : extraParams.substring(0, secondSeparator);
            String farColorStr = extraParams.substring(secondSeparator + 1);

            nearColorStr.trim();
            farColorStr.trim();
            
            if(!nearColorStr.isEmpty())
            {
                colorClose = parseColor(nearColorStr);
            }
            if(!farColorStr.isEmpty())
            {
                colorFar = parseColor(farColorStr);
            }
        }
        else
        {
            extraParams.trim();
            colorClose = parseColor(extraParams);
        }
    }
    else
    {
        String durationStr = params;
        durationStr.trim();
        duration = durationStr.toInt();
    }

    if(setContinuous)
    {
        duration = -1;
    }

    ColorByRangeEffect *effect = new ColorByRangeEffect(colorClose, colorFar, 0, 1, duration);
    if (setContinuous)
    {
        ledController->SetContinuousEffect(effect);
    }
    else
    {
        ledController->EnqueueEffect(effect);
    }
}

// sc <color>:<led_index?(default is all leds -1)>
// Examples:
// sc g:5 // Sets LED at index 5 to green
// sc 100-200-50 // Sets the default LED to RGB(100,200,50)
// sc r // Sets the all LEDs to red
// sc b:5:500 // Sets LED at index 5 to blue for 500ms
// sc b::500 // Sets all leds to blue for 500ms
void PlayEffectCommand::handleSetColor(const String &params, bool setContinuous)
{
    int led = -1;
    unsigned long duration = 1;

    int firstParamSeparator = params.indexOf(PARAM_SEPARATOR);
    String colorStr = params;
    String extraParams;

    if (firstParamSeparator != -1)
    {
        colorStr = params.substring(0, firstParamSeparator);
        extraParams = params.substring(firstParamSeparator + 1);
    }

    colorStr.trim();
    uint32_t color = parseColor(colorStr);

    // Process extra parameters if any
    if (!extraParams.isEmpty())
    {
        int nextParamSeparator = extraParams.indexOf(PARAM_SEPARATOR);
        String ledStr = extraParams;
        String durationStr;

        if (nextParamSeparator != -1)
        {
            ledStr = extraParams.substring(0, nextParamSeparator);
            durationStr = extraParams.substring(nextParamSeparator + 1);
        }

        led = ledStr.isEmpty() ? -1 : ledStr.toInt();
        if (!durationStr.isEmpty())
        {
            duration = durationStr.toInt();
        }
    }

    SetColorEffect *effect = new SetColorEffect(color, led, duration);
    if (setContinuous)
    {
        ledController->SetContinuousEffect(effect);
    }
    else
    {
        ledController->EnqueueEffect(effect);
    }
}

// rc <interval_ms?(default 102ms)>
//NOTE: Interval here is between color updates.
// Examples:
// rc 50 // Starts a rainbow cycle with 50ms interval
// rc // Starts a rainbow cycle with default 102ms interval
void PlayEffectCommand::handleRainbowCycle(const String &command, bool setContinuous)
{
    String params = getParameters(command);

    int interval = 102;
    if(!params.isEmpty())
    {
        interval = params.toInt();
    }

    if(setContinuous)
    {
        interval = -102;
    }

    RainbowCycleEffect *effect = new RainbowCycleEffect(interval);
    if (setContinuous)
    {
        ledController->SetContinuousEffect(effect);
    }
    else
    {
        ledController->EnqueueEffect(effect);
    }
}

 //Example blink continuously red 50ms interval indefinitely: l c bl r:50:-1
void PlayEffectCommand::execute(const String &command)
{
    //command is: l c bl r:50:10
    String commandParams = getParameters(command);//c bl r:50:10
    int firstSpace = commandParams.indexOf(' ');

    String action = commandParams.substring(0, firstSpace);//c
    String params = commandParams.substring(firstSpace + 1);//bl r:50:10

    bool clearContinuous = action == "r";
    if (clearContinuous)
    {
        Serial.println("Clear continuous");
        ledController->RemoveContinuousEffect();
        ledController->SetAllLeds(0);
        return;
    }

    bool setContinuous = action == "c";
    if (setContinuous)
    {
        firstSpace = params.indexOf(' ');
        action = params.substring(0, firstSpace);//bl
        params = params.substring(firstSpace + 1);//r:50:10
    }

    if (action == "bl")
    {
        handleBlink(params, setContinuous);
    }
    else if (action == "sc")
    {
        handleSetColor(params, setContinuous);
    }
    else if (action == "rg")
    {
        handleRange(params, setContinuous);
    }
    else if (action == "rc")
    {
        handleRainbowCycle(params, setContinuous);
    }
}
