#include "LedController.h"

bool LedController::isDisplayingQueuedEffects()
{
    return currentEffect != nullptr || effectQueue.Any();
}

void LedController::moveToNextEffect()
{
    if (!effectQueue.Any())
    {
        return;
    }
 
    currentEffect = effectQueue.Dequeue();

    if (currentEffect)
    {
        currentEffect->Setup(&strip);
    }
}

void LedController::freeCurrentEffect()
{
    delete currentEffect;
    currentEffect = nullptr;
}

void LedController::updateContinuousEffect()
{
    if (continuousEffect == nullptr)
    {
        return;
    }

    if(m_updatedQueuedEffectLastFrame)
    {
        continuousEffect->Resume();
        m_updatedQueuedEffectLastFrame = false;
    }

    continuousEffect->Update();
}

void LedController::Init()
{
    strip.begin();
    strip.show();
    strip.setBrightness(255);
    SetAllLeds(0);
}

void LedController::RemoveContinuousEffect()
{
    if (continuousEffect == NULL)
    {
        return;
    }

    delete continuousEffect;
    continuousEffect = NULL;
}

void LedController::SetContinuousEffect(ILedEffect *effect)
{
    RemoveContinuousEffect();

    continuousEffect = effect;
    continuousEffect->Setup(&strip);
}

void LedController::SetAllLeds(uint32_t color)
{
    if (isDisplayingQueuedEffects())
    {
        return;
    }

    strip.fill(Adafruit_NeoPixel::gamma32(color));
    strip.show();
}

void LedController::SetLed(uint16_t led, uint32_t color)
{
    if (isDisplayingQueuedEffects())
    {
        return;
    }

    strip.setPixelColor(led, Adafruit_NeoPixel::gamma32(color));
    strip.show();
}

void LedController::SetBrightness(uint8_t brt)
{
    strip.setBrightness(brt);
}

bool LedController::EnqueueEffect(ILedEffect *effect)
{
    return effectQueue.Enqueue(effect);
}

void LedController::Update()
{
    //Current effect ended, move next
    if (currentEffect != nullptr && currentEffect->Ended())
    {
        freeCurrentEffect();
        moveToNextEffect();
    }

    //No current effect but theres a queued effect, start it
    if (currentEffect == nullptr && effectQueue.Any())
    {
        moveToNextEffect();
    }

    //No current effect, update continuous if any
    if (currentEffect == nullptr)
    {
        updateContinuousEffect();
        return;
    }

    currentEffect->Update();
    m_updatedQueuedEffectLastFrame = true;
}