#include <Arduino.h>

#include "Commands/Command.h"
#include "Leds/LedController.h"
#include "Leds/LedEffects.h"

#include "Adafruit_NeoPixel.h"

#pragma once

class PlayEffectCommand : public Command
{
private:
    static constexpr char PARAM_SEPARATOR = ':';
    LedController *ledController;

    uint32_t parseColor(const String &color);
    void handleBlink(const String &params, bool setContinuous);
    void handleSetColor(const String &params, bool continuous);
    void handleRainbowCycle(const String &params, bool setContinuous);
    void handleRange(const String &params, bool setContinuous);

public:
    PlayEffectCommand(LedController *ledController);
    void execute(const String &command) override;

    static inline uint32_t ColorRed() { return Adafruit_NeoPixel::Color(255, 0, 0); }
    static inline uint32_t ColorGreen() { return Adafruit_NeoPixel::Color(0, 255, 0); }
    static inline uint32_t ColorBlue() { return Adafruit_NeoPixel::Color(0, 0, 255); }
    static inline uint32_t ColorYellow() { return Adafruit_NeoPixel::Color(255, 255, 0); }
    static inline uint32_t ColorMagenta() { return Adafruit_NeoPixel::Color(255, 0, 255); }
    static inline uint32_t ColorCyan() { return Adafruit_NeoPixel::Color(0, 255, 255); }
    static inline uint32_t ColorWhite() { return Adafruit_NeoPixel::Color(255, 255, 255); }
};
