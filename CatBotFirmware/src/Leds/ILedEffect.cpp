#include "ILedEffect.h"

void ILedEffect::Setup(Adafruit_NeoPixel* strip)
{
    this->strip = strip;
    Start();
}

void ILedEffect::setAllLeds(uint32_t color) 
{
    strip->fill(Adafruit_NeoPixel::gamma32(color));
    strip->show();
}

void ILedEffect::setPixel(uint16_t pixel, uint32_t color)
{
    strip->setPixelColor(pixel, Adafruit_NeoPixel::gamma32(color));
    strip->show();
}
