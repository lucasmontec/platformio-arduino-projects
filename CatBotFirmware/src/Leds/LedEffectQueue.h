#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "ILedEffect.h"

#pragma once

class LedEffectQueue
{
private:
  static const uint8_t maxQueueSize = 10;
  ILedEffect* effectQueue[maxQueueSize];
  uint8_t queueHead = 0;
  uint8_t queueCount = 0;

public:
  bool Enqueue(ILedEffect *effect)
  {
    if (queueCount >= maxQueueSize)
      return false;

    uint8_t queueTail = (queueHead + queueCount) % maxQueueSize;
    effectQueue[queueTail] = effect;
    queueCount++;
    return true;
  }

  ILedEffect *Dequeue()
  {
    if (queueCount == 0)
      return nullptr;

    ILedEffect *effect = effectQueue[queueHead];
    effectQueue[queueHead] = nullptr;
    queueHead = (queueHead + 1) % maxQueueSize;
    queueCount--;

    return effect;
  }

  ILedEffect *Peek() const
  {
    if (queueCount == 0)
      return nullptr;

    return effectQueue[queueHead];
  }

  uint8_t Count() const
  {
    return queueCount;
  }

  bool Any() const
  {
    return Count() > 0;
  }
};