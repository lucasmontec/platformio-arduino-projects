#include "ILedEffect.h"
#include "Sensors/LaserRangeController.h"
#include "ColorUtility.h"

#pragma once

class ColorByRangeEffect : public ILedEffect
{
private:
    uint8_t ledLeft = 0;
    uint8_t ledRight = 1;

    uint32_t colorClose;
    uint32_t colorFar;

    unsigned long duration;
    unsigned long startTime;

    LaserRangeController& laserRangeController = LaserRangeController::getInstance();

    // Max distance in millimeters for color scaling
    static constexpr uint16_t maxDistanceMM = 255;

    static constexpr uint16_t updateInterval = 100;

    unsigned long nextUpdate;

protected:
    virtual void Start() override
    {
        startTime = millis();
        nextUpdate = startTime;
    }

public:
    ColorByRangeEffect(
        uint32_t colorClose,
        uint32_t colorFar,
        uint8_t ledLeft = 0,
        uint8_t ledRight = 1,
        unsigned long duration = 1000)
        : colorClose(colorClose), colorFar(colorFar), duration(duration), ledLeft(ledLeft), ledRight(ledRight) {}

    void Update() override
    {
        if(nextUpdate > millis())
        {
            return;
        }
        nextUpdate = millis() + updateInterval;

        uint16_t leftRange = laserRangeController.SingleShotRangeLeft();
        uint16_t rightRange = laserRangeController.SingleShotRangeRight();

        float ratioLeft = static_cast<float>(leftRange) / maxDistanceMM;
        float ratioRight = static_cast<float>(rightRange) / maxDistanceMM;

        //Serial.println("Ranges left "+String(leftRange)+"mm right "+String(rightRange)+"mm");

        setPixel(ledLeft, ColorUtility::Interpolate(colorClose, colorFar, ratioLeft));
        setPixel(ledRight, ColorUtility::Interpolate(colorClose, colorFar, ratioRight));
    }

    bool Ended() override
    {
        if (duration <= 0)
        {
            return true;
        }

        unsigned long currentTime = millis();
        unsigned long elapsedTime = currentTime - startTime;

        if (elapsedTime >= duration)
        {
            return true;
        }

        return false;
    }
};

class SetColorEffect : public ILedEffect
{
private:
    int led = -1;
    uint32_t color;
    unsigned long duration;
    unsigned long startTime;

protected:
    virtual void Start() override
    {
        startTime = millis();
        Resume();
    }

    virtual void Resume() override
    {
        if (led < 0)
        {
            setAllLeds(color);
            return;
        }
        
        setPixel(led, color);
    }

public:
    SetColorEffect(
        uint32_t color,
        int led = -1,
        unsigned long duration = 1)
        : color(color), duration(duration), led(led) {}

    bool Ended() override
    {
        if(duration <= 0)
        {
            return true;
        }

        unsigned long currentTime = millis();
        unsigned long elapsedTime = currentTime - startTime;

        return elapsedTime >= duration;
    }
};

class BlinkEffect : public ILedEffect
{
private:
    uint32_t offColor;
    uint32_t onColor;
    unsigned long onTime;
    unsigned long offTime;
    int blinkCount;
    int remainingBlinks;
    unsigned long lastBlinkTime;
    bool isOn;

protected:
    virtual void Start() override
    {
        remainingBlinks = blinkCount;
        lastBlinkTime = millis();
        isOn = true;
        setAllLeds(onColor);
    }

public:
    BlinkEffect(
        uint32_t color = Adafruit_NeoPixel::Color(0, 0, 255),
        uint32_t offColor = 0,
        unsigned long onTime = 500,
        unsigned long offTime = 500,
        int blinkCount = 2)
        : onColor(color), offColor(offColor), onTime(onTime), offTime(offTime), blinkCount(blinkCount) {}

    BlinkEffect(
        uint32_t color = Adafruit_NeoPixel::Color(0, 0, 255),
        uint32_t offColor = 0,
        unsigned long onTime = 500,
        int blinkCount = 2)
        : onColor(color), offColor(offColor), onTime(onTime), offTime(onTime), blinkCount(blinkCount) {}

    void Update() override
    {
        if (remainingBlinks == 0)//<0 infinite on purpouse
        {
            return;
        }

        unsigned long currentTime = millis();
        unsigned long elapsedTime = currentTime - lastBlinkTime;
        unsigned long timeThreshold = isOn ? onTime : offTime;

        if (elapsedTime >= timeThreshold)
        {
            isOn = !isOn;
            uint32_t color = isOn ? onColor : offColor;
            setAllLeds(color);
            lastBlinkTime = currentTime;

            if (!isOn && remainingBlinks > 0)
            {
                remainingBlinks--;
            }
        }
    }

    bool Ended() override
    {
        unsigned long currentTime = millis();

        return remainingBlinks <= 0 &&
               currentTime - lastBlinkTime >= offTime;
    }
};

class RainbowCycleEffect : public ILedEffect
{
private:
    unsigned long previousMillis = 0;
    int interval;
    uint16_t currentPixel = 0;
    uint16_t cycleCount;
    bool ended = false;
    bool isContinuous;

protected:
    void Start() override
    {
        previousMillis = millis();
        currentPixel = 0;
        cycleCount = 0;
        ended = false;
    }

public:
    explicit RainbowCycleEffect(int interval = 20) : 
        interval(abs(interval)), isContinuous(interval < 0) {}

    bool Ended() override
    {
        return ended;
    }

    void Update() override
    {
        unsigned long currentMillis = millis();
        if (currentMillis - previousMillis >= interval)
        {
            previousMillis = currentMillis;
            if (++currentPixel >= NumPixels())
            {
                currentPixel = 0;
                if (++cycleCount >= 256 * 5)
                {
                    cycleCount = 0;
                    ended = true;
                    if(!isContinuous)
                    {
                        setAllLeds(0);
                    }
                }
            }
            if (!ended || isContinuous) 
            {

                setPixel(currentPixel, Wheel(((currentPixel * 256 / NumPixels()) + cycleCount) & 255));
                strip->show();
            }
        }
    }

private:
    uint32_t Wheel(byte WheelPos)
    {
        WheelPos = 255 - WheelPos;
        if (WheelPos < 85)
        {
            return Adafruit_NeoPixel::Color(255 - WheelPos * 3, 0, WheelPos * 3);
        }

        if (WheelPos < 170)
        {
            WheelPos -= 85;
            return Adafruit_NeoPixel::Color(0, WheelPos * 3, 255 - WheelPos * 3);
        }

        WheelPos -= 170;
        return Adafruit_NeoPixel::Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    }
};