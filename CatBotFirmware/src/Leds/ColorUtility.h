#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#pragma once

class ColorUtility
{
public:
    static uint8_t GetRed(uint32_t color) { return (color >> 16) & 0xFF; }
    static uint8_t GetGreen(uint32_t color) { return (color >> 8) & 0xFF; }
    static uint8_t GetBlue(uint32_t color) { return color & 0xFF; }
 
    static uint32_t Interpolate(uint32_t color1, uint32_t color2, float ratio) {
        uint8_t red = static_cast<uint8_t>(GetRed(color1) + ratio * (GetRed(color2) - GetRed(color1)));
        uint8_t green = static_cast<uint8_t>(GetGreen(color1) + ratio * (GetGreen(color2) - GetGreen(color1)));
        uint8_t blue = static_cast<uint8_t>(GetBlue(color1) + ratio * (GetBlue(color2) - GetBlue(color1)));

        return Adafruit_NeoPixel::Color(red, green, blue);
    }
};

