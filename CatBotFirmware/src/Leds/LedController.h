#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "LedEffectQueue.h"
#include "LedEffects.h"

#pragma once

class LedController
{
private:
  Adafruit_NeoPixel strip;
  ILedEffect *continuousEffect = nullptr;
  ILedEffect *currentEffect = nullptr;
  LedEffectQueue effectQueue;
  bool m_updatedQueuedEffectLastFrame;

  bool isDisplayingQueuedEffects();
  void moveToNextEffect();
  void updateContinuousEffect();
  void freeCurrentEffect();

public:
  // Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800);
  LedController(uint16_t numPixels, uint8_t pin, neoPixelType type)
      : strip(numPixels, pin, type) {}

  void Init();
  void RemoveContinuousEffect();
  void SetContinuousEffect(ILedEffect *effect);
  void SetAllLeds(uint32_t color);
  void SetLed(uint16_t led, uint32_t color);
  void SetBrightness(uint8_t brt);
  bool EnqueueEffect(ILedEffect *effect);
  void Update();
};