#include <Arduino.h>

#pragma once

class ICommandExecutor {
public:
    virtual void Execute(const String& command) = 0;
};