#include "Command.h"

#include "RangeCommands.h"
#include "StatusCommands.h"

#pragma once

TakeRangeCommand takeRange;
StatusBatteryPercentCommand statusBatteryCommand;

static Command* const commands[] = { 
    &takeRange,
    &statusBatteryCommand
};

static const int commandCount = 2;