#include <Arduino.h>

#include "Command.h"
#include "Sensors/Battery.h"
#include "Status.h"

#pragma once

class StatusBatteryPercentCommand : public Command
{
private:
    BatteryMonitor* batteryMonitor;

public:
    StatusBatteryPercentCommand() : Command("stat_btt")
    {
        batteryMonitor = BatteryMonitor::getInstance();
    }

    void execute(const String& command) override
    {
        String parameters = getParameters(command);

        Serial.println("Battery status requested.");
        
        if(parameters.startsWith("p"))
        {
            globalStatus = String(batteryMonitor->getPercent())+"%";
            Serial.println(globalStatus);
            return;
        }

        if(parameters.startsWith("v"))
        {
            globalStatus = String(batteryMonitor->getVoltage())+"V";
            Serial.println(globalStatus);
            return;
        }

        globalStatus = batteryMonitor->fullStatus();
        Serial.println(globalStatus);
    }
};