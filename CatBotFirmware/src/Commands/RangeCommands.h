#include <Arduino.h>

#include "Command.h"
#include "Sensors/LaserRangeController.h"

#pragma once

class TakeRangeCommand : public Command
{
private:
    LaserRangeController *laserRanger;

public:
    TakeRangeCommand() : Command("srng")
    {
        this->laserRanger = &LaserRangeController::getInstance();
    }

    void execute(const String &command) override
    {
        String parameters = getParameters(command);

        if (parameters == "l")
        {
            laserRanger->StartRangingLeft();
            return;
        }

        if (parameters == "r")
        {
            laserRanger->StartRangingRight();
            return;
        }

        if (parameters == "stp")
        {
            laserRanger->StopRangingLeft();
            laserRanger->StopRangingRight();
            return;
        }

        laserRanger->StartRangingLeft();
        laserRanger->StartRangingRight();
    }
};