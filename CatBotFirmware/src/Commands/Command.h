#include <Arduino.h>

#pragma once

class Command
{
protected:
    String name;

public:
    Command(const String &name);

    virtual void execute(const String &command) = 0;

    bool matches(const String &command);

    String getParameters(const String &command);

    String getName();
};
