#include "Command.h"

Command::Command(const String &name) : name(name) {}

bool Command::matches(const String &command)
{
    int spaceIndex = command.indexOf(' ');
    if (spaceIndex == -1)
    { // No space found, compare the whole command
        return command.equals(name);
    }
    else
    { // Compare the start of the command up to the first space
        return command.startsWith(name) && command.charAt(name.length()) == ' ';
    }
}

String Command::getParameters(const String &command)
{
    String params = command.substring(name.length());
    params.trim();
    return params;
}

String Command::getName()
{
    return name;
}
