#include <Arduino.h>
#include "Command.h"
#include "ICommandExecutor.h"
#include <ArxContainer.h>

#pragma once

#include <Arduino.h>
#include <vector>
#include "Command.h"
#include "ICommandExecutor.h"

class CommandExecutor : public ICommandExecutor
{
private:
    std::vector<Command *> commands;

public:
    CommandExecutor() {}

    CommandExecutor(Command* const* commandsArray, size_t count) {
        for (size_t i = 0; i < count; i++) {
            commands.push_back(commandsArray[i]);
        }
    }

    void addCommand(Command *command)
    {
        commands.push_back(command);
    }

    void removeCommand(Command *command)
    {
        commands.erase(std::remove(commands.begin(), commands.end(), command), commands.end());
    }

    void Execute(const String &input) override
    {
        for (Command *cmd : commands)
        {
            if (!cmd->matches(input))
            {
                continue;
            }
            cmd->execute(input);
        }
    }
};
