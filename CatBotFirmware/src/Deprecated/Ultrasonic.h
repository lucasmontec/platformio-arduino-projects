#include <Arduino.h>
#include <Wire.h>

#define RANGING_DELAY 80L

#pragma once

class UltrasonicSensorsController {
private:
    const byte rangeCommand = byte(0x51);
    const byte leftSensorAddress = byte(0x70);
    const byte rightSensorAddress = byte(0x71);

    int sdaPin = 6;
    int sclPin = 5;
    long i2cFreq = 250000;

    ulong rangingLeftStartMillis;
    ulong rangingRightStartMillis;
    bool rangingLeft = false;
    bool rangingRight = false;

    int leftRange = -1;
    int rightRange = -1;

    int takeRangeReading(const byte address)
    {
        Wire.beginTransmission(address);
        Wire.write(rangeCommand);
        return Wire.endTransmission();
    }

    word requestRange(const byte address)
    {
        Wire.requestFrom(address, byte(2));
        if(Wire.available() >= 2)
        { 
            byte HighByte = Wire.read();
            byte LowByte = Wire.read();
            word range = word(HighByte, LowByte);
            return range;
        }
        else 
        {
            return word(0);
        }
    }

    void checkRangingDone(bool& rangingStatus, ulong& rangingStartTime, const byte sensorAddress, int& resultStore)
    {
        if(!rangingStatus)
        {
            return;
        }

        ulong rangingEnd = rangingStartTime + RANGING_DELAY;
        if(millis() < rangingEnd)
        {
            return;
        }
        
        rangingStartTime = 0L;
        rangingStatus = false;
        resultStore = requestRange(sensorAddress);
    }

    int startRanging(const byte sensorAddress, bool& rangingStatus, ulong& rangingStart)
    {
        int result = takeRangeReading(sensorAddress);

        if(result == 0)
        {
            rangingStatus = true;
            rangingStart = millis();
        }
        return result;
    }

public:
    static UltrasonicSensorsController& getInstance() {
        static UltrasonicSensorsController instance;
        return instance;
    }

    void Init() {
        Wire.begin(sdaPin, sclPin, i2cFreq);
        delay(50);
    }

    int StartRangingLeft()
    {
        return startRanging(leftSensorAddress, rangingLeft, rangingLeftStartMillis);
    }

    int StartRangingRight()
    {
        return startRanging(rightSensorAddress, rangingRight, rangingRightStartMillis);
    }

    bool IsRangingLeft()
    {
        return rangingLeft;
    }

    bool IsRangingRight()
    {
        return rangingLeft;
    }

    //returns -1 if range was never requested
    int GetLeftRange()
    {
        return leftRange;
    }

    //returns -1 if range was never requested
    int GetRightRange()
    {
        return rightRange;
    }

    void Update()
    {
        checkRangingDone(rangingLeft, rangingLeftStartMillis, leftSensorAddress, leftRange);
        checkRangingDone(rangingRight, rangingRightStartMillis, rightSensorAddress, rightRange);
    }

    UltrasonicSensorsController(UltrasonicSensorsController const&) = delete;
    void operator=(UltrasonicSensorsController const&) = delete;
private:
    UltrasonicSensorsController() {} 
};