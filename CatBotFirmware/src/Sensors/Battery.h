#include "MAX1704X.h"

#pragma once

class BatteryMonitor
{
private:
    static BatteryMonitor* instance;
    MAX1704X maxDevice = MAX1704X(MAX17043_mV);
    int lastPercent;
    int init_retries = 5;

    bool init_fault = true;

    BatteryMonitor() {}

public:
    BatteryMonitor(const BatteryMonitor&) = delete;
    BatteryMonitor& operator=(const BatteryMonitor&) = delete;

    static BatteryMonitor* getInstance()
    {
        if (instance == nullptr) 
        {
            instance = new BatteryMonitor();
        }
        return instance;
    }

    void Init()
    {
        init_retries = 5;

        while (!maxDevice.begin(&Wire) && init_retries >= 0) 
        {
            Serial.println(F("Couldnt find MAX1704x!"));
            delay(1000);
            init_retries--;
        }

        if(init_retries < 0)
        {
            init_fault = true;
            return;
        }
        init_fault = false;

        maxDevice.reset();
        maxDevice.quickstart();

        getPercent();
    }

    bool initializationFault()
    {
        return init_fault;
    }

    int getPercent()
    {
        if(init_fault)
        {
            return -1;
        }

        //maxDevice.wake();
        float percent = maxDevice.percent(true);
    	//maxDevice.sleep();
        lastPercent = (int)percent;
        return (int)percent;
    }

    float getVoltage()
    {
        if(init_fault)
        {
            return 0;
        }

        //maxDevice.wake();
        float voltage = maxDevice.voltage();
        //maxDevice.sleep();

        return voltage;
    }

    String fullStatus()
    {
        return "v:"+String(maxDevice.voltage())+"mV p:"+String(maxDevice.percent());
    }

    void printBatteryInfo()
    {
        if(init_fault)
        {
            Serial.print("Initialization failed.");
            return;
        }

        Serial.print("Address:       0x"); Serial.println(maxDevice.address(), HEX);
        Serial.print("ADC:           "); Serial.println(maxDevice.adc());
        Serial.print("Voltage:       "); Serial.print(maxDevice.voltage()); Serial.println(" mV");
        Serial.print("Is Sleeping:   "); Serial.println(maxDevice.isSleeping() ? "Yes" : "No");
        Serial.print("Alert:         "); Serial.println(maxDevice.alertIsActive() ? "Yes" : "No");
        Serial.print("Threshold:     "); Serial.print(maxDevice.getThreshold()); Serial.println("%");
        Serial.print("Battery Percent is "); Serial.print(maxDevice.percent(true)); Serial.println("%");
        Serial.println();
    }
};

// Initialize the static instance pointer
BatteryMonitor* BatteryMonitor::instance = nullptr;