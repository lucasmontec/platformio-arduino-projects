#include "LaserRangeController.h"

LaserRangeController& LaserRangeController::getInstance() 
{
    static LaserRangeController instance;
    return instance;
}

void LaserRangeController::Init() 
{
    pinMode(RANGE_LEFT_SHT_PIN, OUTPUT);
    pinMode(RANGE_RIGHT_SHT_PIN, OUTPUT);

    digitalWrite(RANGE_LEFT_SHT_PIN, LOW);
    digitalWrite(RANGE_RIGHT_SHT_PIN, LOW);
    delay(10);

    digitalWrite(RANGE_LEFT_SHT_PIN, HIGH);
    digitalWrite(RANGE_RIGHT_SHT_PIN, HIGH);
    delay(10);

    digitalWrite(RANGE_LEFT_SHT_PIN, HIGH);
    digitalWrite(RANGE_RIGHT_SHT_PIN, LOW);
    m_rangerLeft.init();
    m_rangerLeft.setAddress(0x30);
    m_rangerLeft.configureDefault(); //scaling 1: 0 - 200mm
    m_rangerLeft.setTimeout(500);
    m_rangerLeft.stopContinuous();
    delay(20);
    Serial.println("Initialized left ranger");

    digitalWrite(RANGE_RIGHT_SHT_PIN, HIGH);
    m_rangerRight.init();
    m_rangerRight.configureDefault(); //scaling 1: 0 - 200mm
    m_rangerRight.setTimeout(500);
    m_rangerRight.stopContinuous();
    Serial.println("Initialized right ranger");
}

bool LaserRangeController::RangingLeft()
{
    return m_continuousLeft;
}

bool LaserRangeController::RangingRight()
{
    return m_continuousRight;
}

void LaserRangeController::StartRangingLeft()
{
    m_continuousLeft = true;
    m_rangerLeft.startInterleavedContinuous(rangeInterval);
}

void LaserRangeController::StartRangingRight()
{
    m_continuousRight = true;
    m_rangerRight.startInterleavedContinuous(rangeInterval);
}

void LaserRangeController::StopRangingLeft()
{
    m_continuousLeft = false;
    m_rangerLeft.stopContinuous();
}

void LaserRangeController::StopRangingRight()
{
    m_continuousRight = false;
    m_rangerRight.stopContinuous();
}

void LaserRangeController::SetRangingInterval(uint interval)
{
    rangeInterval = interval;
    m_rangerRight.setTimeout(rangeInterval);
    m_rangerLeft.setTimeout(rangeInterval);
}

uint16_t LaserRangeController::SingleShotRangeLeft()
{
    m_leftRangeMM = m_rangerLeft.readRangeSingleMillimeters();
    return m_leftRangeMM;
}

uint16_t LaserRangeController::SingleShotRangeRight()
{
    m_rightRangeMM = m_rangerRight.readRangeSingleMillimeters();
    return m_rightRangeMM;
}

int LaserRangeController::GetLeftRange()
{
    return m_leftRangeMM;
}

int LaserRangeController::GetRightRange()
{
    return m_rightRangeMM;
}

void LaserRangeController::Update()
{
    if(m_continuousRight)
    {
        if(m_rangerRight.timeoutOccurred())
        {
            Serial.println("Timeout right");
        }
        else
        {
            m_rightRangeMM = m_rangerRight.readRangeContinuousMillimeters();
        }
    }

    if(m_continuousLeft)
    {
       
        if(m_rangerLeft.timeoutOccurred())
        {
            Serial.println("Timeout left");
        }
        else
        {
            m_leftRangeMM = m_rangerLeft.readRangeContinuousMillimeters();
        }
    }
}

LaserRangeController::LaserRangeController() {} 
