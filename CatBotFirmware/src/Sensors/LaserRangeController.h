#include <Arduino.h>
#include <Wire.h>
#include <VL6180X.h>

#define RANGE_LEFT_SHT_PIN 0
#define RANGE_RIGHT_SHT_PIN 1

#pragma once

class LaserRangeController 
{
private:
    VL6180X m_rangerLeft;
    VL6180X m_rangerRight;

    uint16_t m_leftRangeMM = -1;
    uint16_t m_rightRangeMM = -1;

    bool m_continuousLeft;
    bool m_continuousRight;

    uint rangeInterval = 500;

public:
    static LaserRangeController& getInstance();

    void Init();

    bool RangingLeft();
    bool RangingRight();

    void StartRangingLeft();
    void StartRangingRight();
    void StopRangingLeft();
    void StopRangingRight();

    void SetRangingInterval(uint interval);

    uint16_t SingleShotRangeLeft();
    uint16_t SingleShotRangeRight();

    int GetLeftRange();
    int GetRightRange();

    void Update();

    LaserRangeController(LaserRangeController const&) = delete;
    void operator=(LaserRangeController const&) = delete;

private:
    LaserRangeController();
};