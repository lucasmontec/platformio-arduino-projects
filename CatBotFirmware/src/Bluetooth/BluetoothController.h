#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>

#include "Commands/ICommandExecutor.h"
#include "IControlCommandExecutor.h"
#include "Sensors/Battery.h"
#include "Status.h"

#define CONTROL_SERVICE_UUID "25e1362d-af97-42c1-92e1-41739cc194e6"
#define CONTROL_CHARACTERISTIC_UUID "2a7b25d1-cf73-4b3d-bd95-3a44c1646d1c"
#define RANGE_SENSOR_CHARACTERISTIC_UUID "a9b2ad33-375b-4b9c-9aab-8504c99064e1"
#define STATUS_CHARACTERISTIC_UUID "ab52b456-2d44-443d-99a9-9a8a3b87bfbf"

#define BATTERY_SERVICE_UUID "180F"
#define BATTERY_LEVEL_CHARACTERISTIC_UUID "2A19"

#pragma once

class BatteryCharacteristicCallbacks : public BLECharacteristicCallbacks
{
    void onRead(BLECharacteristic *pCharacteristic)
    {
        BatteryMonitor *monitor = BatteryMonitor::getInstance();

        if (monitor->initializationFault())
        {
            pCharacteristic->setValue("INIT FAULT!");
            return;
        }

        int batteryLevel = monitor->getPercent();
        uint8_t level = (uint8_t)batteryLevel;
        pCharacteristic->setValue(&level, 1);
    }
};

class CatBotControlServerCallbacks : public BLEServerCallbacks
{
    void onConnect(BLEServer *pServer, esp_ble_gatts_cb_param_t *param) override
    {
        Serial.println("Controller connected.");

        uint16_t minInterval = 16; // 20ms (16 * 1.25ms)
        uint16_t maxInterval = 32; // 40ms (32 * 1.25ms)
        uint16_t slaveLatency = 0;
        uint16_t timeout = 400; // Supervision timeout of 4000ms (400 * 10ms)
        // 20ms-40ms interval, 0 slave latency, 4s timeout
        pServer->updateConnParams(param->connect.remote_bda, 16, 32, 0, 400);
    }

    void onDisconnect(BLEServer *pServer)
    {
        BLEDevice::startAdvertising();
        Serial.println("Controller disconnected. Started advertising.");
    }
};

class StatusCharacteristicCallbacks : public BLECharacteristicCallbacks
{
    void onRead(BLECharacteristic *pCharacteristic)
    {
        pCharacteristic->setValue(globalStatus.c_str());
    }
};

class BluetoothCommandController
{
private:
    BLEServer *pServer = nullptr;

    BLECharacteristic *controlCharacteristic = nullptr;
    BLECharacteristic *statusCharacteristic = nullptr;

    IControlCommandExecutor *controlCommandExecutor;
    ICommandExecutor *commandExecutor;

    BatteryMonitor *batteryMonitor;

    String currentControlCommand = "";
    unsigned long currentControlCommandExpireTime = 0;
    const unsigned long commandExpireTime = 200;

    bool isControlCommand(const String *command)
    {
        return command->startsWith("!");
    }

    void executeCurrentControlCommand()
    {
        if (currentControlCommand == "")
        {
            return;
        }

        if (millis() > currentControlCommandExpireTime)
        {
            // controlCommandExecutor->Standby();
            currentControlCommand = "";
            Serial.println("Last command expired.");
            return;
        }

        controlCommandExecutor->Execute(currentControlCommand);
    }

    void processNewCommands()
    {
        if (controlCharacteristic->getValue().length() <= 0)
        {
            return;
        }

        String command = controlCharacteristic->getValue().c_str();

        if (isControlCommand(&command))
        {
            currentControlCommand = command.substring(1); // remove the !
            currentControlCommandExpireTime = millis() + commandExpireTime;
            controlCharacteristic->setValue("");
            return;
        }

        commandExecutor->Execute(command);
        controlCharacteristic->setValue("");
    }

public:
    BluetoothCommandController(IControlCommandExecutor *controlExecutor, ICommandExecutor *configExecutor)
        : controlCommandExecutor(controlExecutor), commandExecutor(configExecutor) {}

    void Init()
    {
        BLEDevice::init("CatBot Control");

        pServer = BLEDevice::createServer();
        pServer->setCallbacks(new CatBotControlServerCallbacks());

        // Control Service
        BLEService *pControlService = pServer->createService(CONTROL_SERVICE_UUID);
        controlCharacteristic = pControlService->createCharacteristic(
            CONTROL_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_WRITE);

        statusCharacteristic = pControlService->createCharacteristic(
            STATUS_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ);
        statusCharacteristic->setCallbacks(new StatusCharacteristicCallbacks());
        pControlService->start();

        // Battery Service
        BLEService *pBatteryService = pServer->createService(BATTERY_SERVICE_UUID);
        BLECharacteristic *batteryLevelCharacteristic = pBatteryService->createCharacteristic(
            BATTERY_LEVEL_CHARACTERISTIC_UUID,
            BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
        batteryLevelCharacteristic->setCallbacks(new BatteryCharacteristicCallbacks());
        pBatteryService->start();

        BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
        pAdvertising->addServiceUUID(CONTROL_SERVICE_UUID);
        pAdvertising->addServiceUUID(BATTERY_SERVICE_UUID);
        pAdvertising->setScanResponse(true);
        pAdvertising->setMinPreferred(0x06);
        pAdvertising->setMinPreferred(0x12);
        BLEDevice::startAdvertising();

        Serial.println("CatBot BLE Control initialized.");
    }

    void UpdateControlService()
    {
        executeCurrentControlCommand();
        processNewCommands();
    }
};