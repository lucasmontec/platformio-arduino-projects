#include <Arduino.h>
#include "Commands/ICommandExecutor.h"

#pragma once

class IControlCommandExecutor : public ICommandExecutor
{
public:
    virtual void Standby() = 0;
};