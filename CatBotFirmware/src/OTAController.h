#include <WiFi.h>
#include <ArduinoOTA.h>

#pragma once

class OTAController {
  private:
    const char* ssid;
    const char* password;
    unsigned long otaDuration;
    unsigned long startTime;
    bool enabled;

  public:
    OTAController(const char* _ssid, const char* _password, unsigned long _otaDuration)
      : ssid(_ssid), password(_password), otaDuration(_otaDuration), enabled(true) {}

    void begin() {
      WiFi.begin(ssid, password);
      Serial.println("[OTA] Connecting to WiFi...");

      while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.println("[OTA] Connecting to WiFi...");
      }
      Serial.println("[OTA] Connected to WiFi");

      ArduinoOTA.begin();

      startTime = millis();
    }

    void Handle() {
      if (!enabled) return;

      ArduinoOTA.handle();

      unsigned long currentTime = millis();
      if ((currentTime - startTime) >= (otaDuration * 1000)) {
        ArduinoOTA.end();
        Serial.println("[OTA] OTA disabled");

        WiFi.disconnect();
        Serial.println("[OTA] WiFi disconnected");

        enabled = false;
      }
    }
};
