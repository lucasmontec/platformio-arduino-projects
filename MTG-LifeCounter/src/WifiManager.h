#ifndef WifiManager_h
#define WifiManager_h

#include <ESP8266WiFi.h>
#include "UIBar.h"
#include "icons.h"

class WifiManager
{
public:
  WifiManager(String ssid, String password, int timeout=8000) : ssid(ssid), password(password), timeout(timeout) {}

  void keepConnection();
  void renderStatusIcon(UIBar *bar);
  bool isConnected();
  void setOnConnected(void (*onConnected)(void));
  void retry();

private:
  String ssid;
  String password;
  bool connecting;
  bool connected;
  void (*onConnected)(void);

  int timeout;
  int currentTryTime;
  bool stopped;
};

#endif
