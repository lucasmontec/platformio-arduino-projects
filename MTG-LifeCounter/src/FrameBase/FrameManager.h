#ifndef FrameManager_h
#define FrameManager_h

#include "Arduino.h"
#include "Frame.h"
#include "BoundedCursor.h"

class FrameManager
{
public:
    FrameManager(BoundedCursor* cur);
    void setFrame(Frame *frame);
    void render(SSD1306Wire *display);

    void inputUp(bool val);
    void inputDown(bool val);
    void inputLeft(bool val);
    void inputRight(bool val);
    void inputSelect(bool val);

private:
    Frame *currentFrame = NULL;
    BoundedCursor *cursor = NULL;
    bool useCursor();
    bool wasClicked;

    int lastCursorX, lastCursorY;

    void handleClicks(bool val);
    void handleCursorMoved();
};

#endif
