#ifndef Button_h
#define Button_h

#include "Arduino.h"
#include "UIBar.h"

class Button
{
public:
    virtual void render(SSD1306Wire *display) = 0;
    virtual void click(int x, int y) = 0;
    virtual void hoverUpdate(int x, int y);
protected:
    bool isHovered;
    int x, y;
};

#endif