#include "UI/TextButton.h"

/*
ArialMT_Plain_10
// Width: 10
// Height: 13

ArialMT_Plain_16
// Width: 16
// Height: 19

ArialMT_Plain_24
// Width: 24
// Height: 28

display.setFont(ArialMT_Plain_10);
*/

TextButton::TextButton(int x, int y, const char* text)
{
    buttonText = text;
    this->x = x;
    this->y = y;
}

void TextButton::render(SSD1306Wire *display)
{
    display->setFont(ArialMT_Plain_10);
    int charCount = strlen(buttonText);
    
    int buttonW = (charCount * 10) + 4; // 10 = ArialMT_Plain_10
    int buttonH = 13 + 2; // 13 = ArialMT_Plain_10

    int textX = x + 2;
    int textY = y + 1;

    display->drawRect(x, y, buttonW, buttonH);
    display->drawString(textX, textY, buttonText);
}

void TextButton::click(int x, int y)
{

}

void TextButton::hoverUpdate(int x, int y)
{

}