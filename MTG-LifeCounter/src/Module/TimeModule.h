#ifndef TimeModule_h
#define TimeModule_h

#include <WiFiUdp.h>
#include <NTPClient.h>
#include "UIBar.h"
#include "TopBarModule.h"

class TimeModule : public TopBarModule
{
public:
    TimeModule();
    TimeModule(int utcOffsetHours);
    void begin();
    void update();
    void render(UIBar *bar);

private:
    WiFiUDP *ntpUDP;
    NTPClient *timeClient;
    long began;
    String timeStr = "00:00";
};

#endif