#include <Arduino.h>
#include <Wire.h>
#include "SSD1306Wire.h"

#include <ArduinoJson.h>

#include "OTA.h"

#include "Input.h"
#include "FrameBase/FrameManager.h"
#include "Frame/MainFrame.h"
#include "UIBar.h"
#include "WifiManager.h"
#include "BoundedCursor.h"

#include "Module/TopBarModule.h"
#include "Module/TimeModule.h"

#define MAX_MODULES 10

//XBM https://xbm.jazzychad.net/
//Display: https://github.com/ThingPulse/esp8266-oled-ssd1306
//OTA: https://www.youtube.com/watch?v=1pwqS_NUG7Q

//Display
SSD1306Wire display(0x3c, D1, D2);

FrameManager* frameManager;
UIBar* topBar;
WifiManager* wifiManager;
Frame* mainFrame;

TimeModule* timeModule = new TimeModule(-3);
TopBarModule* modules[MAX_MODULES] = {timeModule};

void onWifiConnected()
{
  timeModule->begin();
}

void otaProgress(unsigned int progress);

void setup()
{
  setupInput();

  display.init();
  display.flipScreenVertically();

  topBar = new UIBar(&display, 0, 127, 0, 2);

  wifiManager = new WifiManager(String("Fidalga"), String("odinteabencoacomwifi"));

  wifiManager->setOnConnected(&onWifiConnected);

  frameManager = new FrameManager(new BoundedCursor(0, 12, 127, 63));
  frameManager->setFrame(new MainFrame(topBar));

  setupOTA("mtg-device", "8568");
  otaProgressHandler = otaProgress;
  beginOTA();
}

void frameLoop();
bool otaLoop();

void loop()
{
  updateOTA();

  if(isOTAUpdating) return;
  frameLoop();
}

void otaProgress(unsigned int progress)
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(63, 31-10-2-4, "OTA Update"); //31[center_vert], 10 = (5[bar_h/2] + 5[text_h/2]), 2[space]
  display.drawProgressBar(10, 32-5, 127-20, 10, progress);
  display.display();
}

void updateModules()
{
  for(int i=0;i<MAX_MODULES;i++)
  {
    TopBarModule* module = modules[i];
    if(module == NULL) continue;
    module->update();
    module->render(topBar);
  }

/*
  timeModule->update();
  timeModule->render(topBar);
  */
}

void frameLoop()
{
  display.clear();

  processInput(frameManager);

  updateModules();

  wifiManager->keepConnection();
  wifiManager->renderStatusIcon(topBar);

  frameManager->render(&display);

  display.display();
  topBar->reset();
}
