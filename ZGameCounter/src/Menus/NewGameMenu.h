#pragma once

#include <lvgl.h>
#include <Arduino.h>

static lv_obj_t * kb;

static const char* playerNames[5];

void createKeyboard(lv_obj_t* screen)
{
    /*Create a keyboard*/
    kb = lv_keyboard_create(screen);
    lv_obj_set_size(kb,  LV_HOR_RES, LV_VER_RES / 3);

    //lv_keyboard_set_textarea(kb, pwd_ta);
}

static void buttonCallback(lv_event_t * e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* button = lv_event_get_target(e);
    auto callback = (void(*)(const char**))lv_obj_get_user_data(button);  

    if(code == LV_EVENT_CLICKED) {
        callback(playerNames);
    }
}

static void playerInputFieldCallback(lv_event_t * e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t * ta = lv_event_get_target(e);
    auto playerIndex = (int*)lv_obj_get_user_data(ta);  

    if(code == LV_EVENT_CLICKED || code == LV_EVENT_FOCUSED) {
        /*Focus on the clicked text area*/
        if(kb != NULL) lv_keyboard_set_textarea(kb, ta);
    }

    else if(code == LV_EVENT_READY || code == LV_EVENT_VALUE_CHANGED) {
        int playerId = *playerIndex;
        playerNames[playerId] = lv_textarea_get_text(ta);
    }
}

void createPlayerNameField(lv_obj_t* root, int playerId)
{
    String labelText = "Player "+String(playerId)+" name:";
    lv_obj_t * oneline_label = lv_label_create(root);
    lv_label_set_text(oneline_label, labelText.c_str());

    /*Create the one-line mode text area*/
    lv_obj_t * text_ta = lv_textarea_create(root);
    lv_textarea_set_one_line(text_ta, true);
    lv_textarea_set_password_mode(text_ta, false);
    lv_obj_set_width(text_ta, lv_pct(80));
    lv_obj_add_event_cb(text_ta, playerInputFieldCallback, LV_EVENT_ALL, NULL);

    void *playerIdPtr = malloc(sizeof(int));
    *((int*)playerIdPtr) = playerId;

    lv_obj_set_user_data(text_ta, playerIdPtr);
    //lv_obj_align(text_ta, LV_ALIGN_TOP_RIGHT, -5, 20);
}

void createStartGameButton(lv_obj_t* root, void (*callback)(const char**))
{
    lv_obj_t * label;

    lv_obj_t * btn = lv_btn_create(root);
    lv_obj_add_event_cb(btn, buttonCallback, LV_EVENT_ALL, NULL);
    lv_obj_set_width(btn, lv_pct(100));
    //lv_obj_align(btn, LV_ALIGN_CENTER, 0, -40);

    label = lv_label_create(btn);
    lv_label_set_text(label, "Start Game");
    lv_obj_center(label);
    lv_obj_set_user_data(btn, reinterpret_cast<void *&>(callback));
}

void createNewGameScreen(int playerCount, void (*callback)(const char**))
{
    lv_obj_t* screen = lv_obj_create(NULL);

    lv_obj_set_layout(screen, LV_LAYOUT_FLEX);
    lv_obj_set_flex_flow(screen, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_size(screen, LV_PCT(100), LV_PCT(100));

    //lv_obj_set_flex_align(screen, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_START, LV_FLEX_ALIGN_START);

    lv_obj_center(screen);

    for(int i=0; i<playerCount; i++)
    {
        createPlayerNameField(screen, i);
    }

    createStartGameButton(screen, callback);

    createKeyboard(screen);

    lv_scr_load(screen);
}