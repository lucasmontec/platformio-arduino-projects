#pragma once

#include <Arduino.h>

#define PIN_LED_RED 4
#define PIN_LED_GREEN 16
#define PIN_LED_BLUE 17

float color[3];

void setupLed()
{
    pinMode(PIN_LED_RED, OUTPUT);
    pinMode(PIN_LED_GREEN, OUTPUT);
    pinMode(PIN_LED_BLUE, OUTPUT);
}

void writeColor(int r, int g, int b)
{
    analogWrite(PIN_LED_RED, 255-r);
    analogWrite(PIN_LED_GREEN, 255-g);
    analogWrite(PIN_LED_BLUE, 255-b);
}

float fract(float x) { return x - int(x); }

float mix(float a, float b, float t) { return a + (b - a) * t; }

float* hsv2rgb(float h, float s, float b, float* rgb) {
  rgb[0] = b * mix(1.0, constrain(abs(fract(h + 1.0) * 6.0 - 3.0) - 1.0, 0.0, 1.0), s);
  rgb[1] = b * mix(1.0, constrain(abs(fract(h + 0.6666666) * 6.0 - 3.0) - 1.0, 0.0, 1.0), s);
  rgb[2] = b * mix(1.0, constrain(abs(fract(h + 0.3333333) * 6.0 - 3.0) - 1.0, 0.0, 1.0), s);
  return rgb;
}

void writeColorHSV(float h, float s, float v)
{
    hsv2rgb(h, s, v, color);
    writeColor(color[0] * 255, color[1] * 255, color[2] * 255);
}