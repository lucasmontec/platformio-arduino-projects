#pragma once

#include "ZLVSetup.h"
#include "ZLVTouch.h"
#include <Arduino.h>
#include <lvgl.h>
#include <TFT_eSPI.h>

static void ReadTouch(lv_indev_drv_t* indev_driver, lv_indev_data_t* data);
static void FlushDisplay(lv_disp_drv_t* disp, const lv_area_t* area, lv_color_t* color_p);

static lv_disp_draw_buf_t draw_buffer;
static lv_color_t buffer[width * 10];

String LVGL_Version;

TFT_eSPI tft = TFT_eSPI(width, height);

void ZLVFlushDisplay(lv_disp_drv_t* disp, const lv_area_t* area, lv_color_t* color_p)
{
    uint32_t w = ( area->x2 - area->x1 + 1 );
    uint32_t h = ( area->y2 - area->y1 + 1 );

    tft.startWrite();
    tft.setAddrWindow(area->x1, area->y1, w, h );
    tft.pushColors((uint16_t*) &color_p->full, w * h, true);
    tft.endWrite();

    lv_disp_flush_ready(disp);
}

void ZLVCreatePanel() 
{
    LVGL_Version += "LVGL " + String('V') + lv_version_major() + "." + lv_version_minor() + "." + lv_version_patch();

    lv_init();

    tft.begin();/* TFT init */
    //tft.setRotation( 3 ); /* Landscape orientation, flipped */
    /*
    The rotation parameter can be 0, 1, 2 or 3.
    For displays that are part of an Arduino shield, rotation value 0 sets the display to a portrait (tall) mode.
    Rotation value 2 is also a portrait mode. Rotation 1 is landscape (wide) mode, while rotation 3 is also landscape.
    */
    tft.setRotation( 0 );

    lv_disp_draw_buf_init(&draw_buffer, buffer, NULL, width * 10);

    /*Initialize the display*/
    static lv_disp_drv_t disp_drv;
    lv_disp_drv_init( &disp_drv );

    /*Change the following line to your display resolution*/
    disp_drv.hor_res = width;
    disp_drv.ver_res = height;
    disp_drv.flush_cb = ZLVFlushDisplay;
    disp_drv.draw_buf = &draw_buffer;
    lv_disp_drv_register( &disp_drv );

    /*Initialize the (dummy) input device driver*/
    static lv_indev_drv_t indev_drv;
    lv_indev_drv_init( &indev_drv );
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = ZLVReadTouch;
    lv_indev_drv_register( &indev_drv );
}
