#pragma once

#include "ZLVSetup.h"
#include "TAMC_GT911.h"
#include <lvgl.h>

TAMC_GT911 touchPanel = TAMC_GT911(TOUCH_SDA, TOUCH_SCL, TOUCH_INT, TOUCH_RST, TOUCH_WIDTH, TOUCH_HEIGHT);

static bool touchSetup;

void ZLVSetupTouch()
{
    touchSetup = true;
    touchPanel.begin();
    touchPanel.setRotation(ROTATION_RIGHT);
}

void ZLVReadTouch(lv_indev_drv_t* indev_driver, lv_indev_data_t* data)
{
    if(!touchSetup) return;

    touchPanel.read();
    bool touched = touchPanel.isTouched;

    if( !touched )
    {
        data->state = LV_INDEV_STATE_REL;
    }
    else
    {
        data->state = LV_INDEV_STATE_PR;
        data->point.x = width - touchPanel.points[0].y;
        data->point.y = touchPanel.points[0].x;
    }
}