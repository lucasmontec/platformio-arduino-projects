#pragma once

#include <Arduino.h>

#define TOUCH_SDA 33
#define TOUCH_SCL 32
#define TOUCH_INT 21
#define TOUCH_RST 25
#define TOUCH_WIDTH  480
#define TOUCH_HEIGHT 320

static const uint16_t width  = 320;
static const uint16_t height = 480;