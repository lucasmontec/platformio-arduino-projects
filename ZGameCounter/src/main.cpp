#include <Arduino.h>

#include "ZLVGL/ZLVSetup.h"
#include "ZLVGL/ZLVPanel.h"
#include "ZLVGL/ZLVTouch.h"

#include "Led/LedControl.h"

#include "Menus/NewGameMenu.h"

#include "Counter/LifeCounter.h"
#include "Counter/ScrollCounterScreen.h"
#include "Counter/TwoPlayerCounterScreen.h"

bool doneColorDance;

void newGameCallback(const char* playerNames[])
{
    createScrollCounterScreen(40, 4, playerNames);
}

void setup()
{
    ZLVSetupTouch();
    ZLVCreatePanel();
    
    //create2PlayerMtgScreen(40);

    /*
    const char* playerNames[2] = {"Player 1", "Player 2"};
    createScrollCounterScreen(40, 2, playerNames);
    */

    createNewGameScreen(4, newGameCallback);
    
    setupLed();

/*
    const char* playerNames[4] = {"Player 1", "Player 2", "Player 3", "Player 4"};
    createScrollCounterScreen(40, 4, playerNames);
*/
}

void colorDance();

void loop()
{
    if(!doneColorDance)
    {
        colorDance();
        doneColorDance = true;
    }

    lv_task_handler();
    lv_timer_handler();
    delay( 5 );
}

void colorDance()
{
    for(float i=0;i<1;i+=0.05f)
    {
        writeColorHSV(i, 1.0, 1.0);
        delay(70);
    }

    writeColor(0, 0, 0);
}
