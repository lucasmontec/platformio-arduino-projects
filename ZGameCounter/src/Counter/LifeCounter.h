#pragma once

#include <lvgl.h>
#include <Arduino.h>

struct CounterData {
    lv_obj_t* counter_label;
    int count;
};

static void lv_spinbox_increment_event_cb(lv_event_t * e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* button = lv_event_get_target(e);
    auto counterData = (CounterData*)lv_obj_get_user_data(button);

    if(code == LV_EVENT_SHORT_CLICKED || code  == LV_EVENT_LONG_PRESSED_REPEAT) {
        auto label = counterData->counter_label;
        counterData->count++;
        lv_label_set_text(label, String(counterData->count).c_str());
    }
}

static void lv_spinbox_decrement_event_cb(lv_event_t * e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t* button = lv_event_get_target(e);
    auto counterData = (CounterData*)lv_obj_get_user_data(button);

    if(code == LV_EVENT_SHORT_CLICKED || code == LV_EVENT_LONG_PRESSED_REPEAT) {
        auto label = counterData->counter_label;
        counterData->count--;
        lv_label_set_text(label, String(counterData->count).c_str());
    }
}

lv_obj_t* createCounter(int initialValue, const char* playerName, lv_obj_t* parent)
{
    auto root = lv_obj_create(parent);

    static lv_coord_t col_dsc[] = {LV_GRID_FR(1), LV_GRID_FR(1), LV_GRID_FR(1), LV_GRID_TEMPLATE_LAST};
    static lv_coord_t row_dsc[] = {LV_GRID_FR(1), LV_GRID_TEMPLATE_LAST};

    lv_obj_set_grid_dsc_array(root, col_dsc, row_dsc);

    //Center content with life counter and player name
    auto centerContent = lv_obj_create(root);
    lv_obj_clear_flag(centerContent, LV_OBJ_FLAG_SCROLLABLE);
    lv_obj_set_grid_cell(centerContent, LV_GRID_ALIGN_STRETCH, 1, 1, LV_GRID_ALIGN_STRETCH, 0, 1); //grid 1, 0
    lv_obj_set_style_border_width(centerContent, 0, LV_PART_MAIN);

    //Life counter label
    auto label = lv_label_create(centerContent);
    lv_label_set_text(label, String(initialValue).c_str());
    lv_obj_set_style_text_font(label, &lv_font_montserrat_48, 0);
    lv_obj_clear_flag(label, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_center(label);

    //Associated counter data
    CounterData* counterData = new CounterData;
    counterData->count = initialValue;
    counterData->counter_label = label;

    //Player name label
    label = lv_label_create(centerContent);
    lv_label_set_text(label, playerName);
    lv_obj_set_style_text_font(label, &lv_font_montserrat_18, 0);
    lv_obj_clear_flag(label, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_align(label, LV_ALIGN_TOP_MID, 0, 0);

    //Right increment button
    lv_obj_t * btn = lv_btn_create(root);
    lv_obj_set_style_bg_img_src(btn, LV_SYMBOL_PLUS, 0);
    lv_obj_add_event_cb(btn, lv_spinbox_increment_event_cb, LV_EVENT_ALL,  NULL);
    lv_obj_set_user_data(btn, counterData);
    lv_obj_set_grid_cell(btn, LV_GRID_ALIGN_STRETCH, 2, 1, LV_GRID_ALIGN_STRETCH, 0, 1); //grid 2, 0

    //Left decrement button
    btn = lv_btn_create(root);
    lv_obj_set_style_bg_img_src(btn, LV_SYMBOL_MINUS, 0);
    lv_obj_add_event_cb(btn, lv_spinbox_decrement_event_cb, LV_EVENT_ALL, NULL);
    lv_obj_set_user_data(btn, counterData);
    lv_obj_set_grid_cell(btn, LV_GRID_ALIGN_STRETCH, 0, 1, LV_GRID_ALIGN_STRETCH, 0, 1); //grid 0, 0

    return root;
}
