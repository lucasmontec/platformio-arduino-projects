
#pragma once

#include "LifeCounter.h"
#include <Arduino.h>

void createScrollCounterScreen(int initialLife, int playerCount, const char* playerNames[])
{
    lv_obj_t* screen = lv_obj_create(NULL);
    lv_obj_set_layout(screen, LV_LAYOUT_FLEX);
    lv_obj_set_flex_flow(screen, LV_FLEX_FLOW_COLUMN);
    lv_obj_set_size(screen, LV_PCT(100), LV_PCT(100));

    if(playerCount <= 3)
    {
        lv_obj_set_flex_align(screen, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_START, LV_FLEX_ALIGN_START);
    }

    lv_obj_center(screen);

    for (int i = 0; i < playerCount; i++){
        lv_obj_t* counter = createCounter(initialLife, playerNames[i], screen);
        lv_obj_set_size(counter, LV_PCT(100), LV_SIZE_CONTENT);//LV_PCT(100) for 100% percentage
        //lv_obj_set_flex_grow(counter, 1); //Not sure if it would have a minimum size
    }

    lv_scr_load(screen);
}