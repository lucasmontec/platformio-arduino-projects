
#pragma once

#include "LifeCounter.h"
#include <Arduino.h>

void create2PlayerMtgScreen(int initialLife)
{
    static lv_coord_t col_dsc[] = {LV_GRID_FR(1), LV_GRID_TEMPLATE_LAST};
    static lv_coord_t row_dsc[] = {LV_GRID_FR(1), LV_GRID_FR(1), LV_GRID_TEMPLATE_LAST};

    lv_obj_t* screen = lv_obj_create(NULL);
    lv_obj_center(screen);
    lv_obj_set_grid_dsc_array(screen, col_dsc, row_dsc);

    lv_obj_t* counter = createCounter(initialLife, "Player 1", screen);
    lv_obj_set_grid_cell(counter, LV_GRID_ALIGN_STRETCH, 0, 1, LV_GRID_ALIGN_STRETCH, 0, 1);

    lv_obj_set_style_transform_pivot_x(counter, lv_pct(50), 0);
    lv_obj_set_style_transform_pivot_y(counter, lv_pct(50), 0);

    lv_obj_set_style_transform_angle(counter, 1800, 0);

    counter = createCounter(initialLife, "Player 2", screen);
    lv_obj_set_grid_cell(counter, LV_GRID_ALIGN_STRETCH, 0, 1, LV_GRID_ALIGN_STRETCH, 1, 1);

    lv_obj_set_style_transform_pivot_x(counter, lv_pct(50), 0);
    lv_obj_set_style_transform_pivot_y(counter, lv_pct(50), 0);
    
    //lv_obj_set_style_transform_angle(counter, 0, 0);

    lv_scr_load(screen);
}