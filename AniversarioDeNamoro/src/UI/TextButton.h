#ifndef TextButton_h
#define TextButton_h

#include "Arduino.h"
#include "UIBar.h"
#include "UI/Button.h"

class TextButton : public Button
{
public:
    TextButton(int x, int y, String text, void (*action)(void) = NULL);
    void render(SSD1306Wire *display) override;
    void click(int x, int y) override;
    void hoverUpdate(int x, int y) override;
private:
    String buttonText;
};

#endif