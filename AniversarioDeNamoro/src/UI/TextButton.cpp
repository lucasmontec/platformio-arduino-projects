#include "UI/TextButton.h"

/*
ArialMT_Plain_10
// Width: 10
// Height: 13

ArialMT_Plain_16
// Width: 16
// Height: 19

ArialMT_Plain_24
// Width: 24
// Height: 28

display.setFont(ArialMT_Plain_10);
*/

TextButton::TextButton(int x, int y, String text, void (*action)(void))
{
    buttonText = text;
    this->x = x;
    this->y = y;

    int charCount = text.length();
    height = 13 + 2; // 13 = ArialMT_Plain_10

    this->action = action;
}

void TextButton::render(SSD1306Wire *display)
{
    display->setFont(ArialMT_Plain_10);
    
    int textX = x + 2;
    int textY = y + 1;

    if(width == 0)
    {
        width = display->getStringWidth(buttonText) + 4;
    }

    display->setTextAlignment(TEXT_ALIGN_LEFT);
    display->drawRect(x, y, width, height);
    display->drawString(textX, textY, buttonText);
}

void TextButton::click(int x, int y)
{
    if(x < this->x || x > this->x + this->width)
    {
        return;
    }

    if(y < this->y || y > this->y + this->height)
    {
        return;
    }

    if (action != 0)
    {
        action();
    }
}

void TextButton::hoverUpdate(int x, int y)
{

}