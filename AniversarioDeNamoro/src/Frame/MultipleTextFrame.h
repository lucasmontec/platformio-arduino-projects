#pragma once

#include "Arduino.h"
#include "FrameBase/Frame.h"
#include "UIBar.h"
#include "UI/TextButton.h"

class MultipleTextFrame : public Frame
{
public:
  MultipleTextFrame(UIBar *bar, const char* texts[], int count, void (*endAction)(void)) : Frame(bar)
  {
    this->count = count;
    this->texts = texts;
    this->endAction = endAction;

    text = String(texts[0]);
  }

  void render(SSD1306Wire *display)
  {
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    display->drawString(63, 31, text);
  }

  void nextText()
  {
    currentText++;
    if(currentText >= count)
    {
      currentText = 0;
      if(endAction != 0)
      {
        endAction();
      }
      else
      {
        return;
      }
    }

    text = String(texts[currentText]);
  }

private:
    const char** texts;
    String text;

    int count;
    int currentText = 0;

    void (*endAction)(void);
};