#pragma once

#include "Arduino.h"
#include "FrameBase/Frame.h"
#include "UIBar.h"
#include "UI/TextButton.h"

class QuestionFrame : public Frame
{
public:
  QuestionFrame(UIBar *bar, const char* text, void (*correctAction)(void), void (*wrongAction)(void), const char* correctText="sim", const char* wrongText="nao") : Frame(bar)
  {
    this->text = text;
    this->addButton(new TextButton(30, 48, correctText, correctAction));
    this->addButton(new TextButton(75, 48, wrongText, wrongAction));
  }

  void render(SSD1306Wire *display)
  {
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    display->drawString(63, 28, text);
  }

private:
    String text;
};