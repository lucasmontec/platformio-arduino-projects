#pragma once

#include "Arduino.h"
#include "FrameBase/Frame.h"
#include "UIBar.h"

class TextFrame : public Frame
{
public:
  TextFrame(UIBar *bar, String text) : Frame(bar)
  {
    this->text = text;
  }

  void render(SSD1306Wire *display)
  {
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    display->drawString(63, 31, text);
  }

private:
    String text;
};