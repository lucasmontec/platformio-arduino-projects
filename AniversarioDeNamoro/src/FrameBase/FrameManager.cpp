#include "FrameManager.h"

FrameManager::FrameManager(BoundedCursor *cur)
{
    cursor = cur;
}

void FrameManager::setFrame(Frame *frame)
{
    if (currentFrame)
    {
        currentFrame->end();
    }

    currentFrame = frame;

    if (currentFrame)
    {
        currentFrame->begin();
    }
}

void FrameManager::render(SSD1306Wire *display)
{
    if (currentFrame)
    {
        currentFrame->render(display);

        renderButtons(display);
    }

    if (useCursor())
    {
        cursor->render(display);
    }
}

void FrameManager::renderButtons(SSD1306Wire *display)
{
    if (!currentFrame)
    {
        return;
    }

    for(int i = 0; i < currentFrame->getButtonCount(); i++)
    {
        currentFrame->buttons[i]->render(display);
    }
}

void FrameManager::handleCursorMoved()
{
    if(!currentFrame)
    {
        return;
    }

    if(cursor->getX() != lastCursorX || cursor->getY() != lastCursorY)
    {
        lastCursorX = cursor->getX();
        lastCursorY = cursor->getY();
        currentFrame->onCursorMoved(cursor->getX(), cursor->getY());
    }
}

void FrameManager::inputUp(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputUp(val);
    }

    if (useCursor() && val)
    {
        cursor->moveUp();
    }
}

void FrameManager::inputDown(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputDown(val);
    }

    if (useCursor() && val)
    {
        cursor->moveDown();
    }
}

void FrameManager::inputLeft(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputLeft(val);
    }

    if (useCursor() && val)
    {
        cursor->moveLeft();
    }
}

void FrameManager::inputRight(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputRight(val);
    }

    if (useCursor() && val)
    {
        cursor->moveRight();
    }
}

void FrameManager::inputSelect(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputSelect(val);
    }

    if (useCursor())
    {
        if (val)
        {
            cursor->setSelected();
        }
        else
        {
            cursor->setReleased();
        }
    }

    handleClicks(val);
}

void FrameManager::handleClicks(bool val)
{
    if (!currentFrame)
    {
        return;
    }

    if (val)
    {
        //debounce
        if(millis() < nextClickTime)
        {
            return;
        }
        nextClickTime = millis() + 250;

        if (!wasClicked)
        {
            currentFrame->onClicked(cursor->getX(), cursor->getY());
            updateButtonClicks(cursor->getX(), cursor->getY());
            wasClicked = true;
        }
    }
    else
    {
        if (wasClicked)
        {
            currentFrame->onReleased(cursor->getX(), cursor->getY());
            wasClicked = false;
        }
    }
}

void FrameManager::updateButtonClicks(int x, int y)
{
    if (!currentFrame)
    {
        return;
    }

    for(int i = 0; i < currentFrame->getButtonCount(); i++)
    {
        currentFrame->buttons[i]->click(x, y);
    }
}

bool FrameManager::useCursor()
{
    return cursor && currentFrame && currentFrame->useCursor;
}