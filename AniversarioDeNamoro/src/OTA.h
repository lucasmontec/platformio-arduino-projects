#ifndef OTA_h
#define OTA_h

#include <ArduinoOTA.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

bool beganOTA;
bool isOTAUpdating;
bool isOTAError;
String OTAError;

void (*otaProgressHandler)(unsigned int);

void setupOTA(const char *hostname, const char *password, unsigned int port = 8266)
{
    ArduinoOTA.setPort(port);
    ArduinoOTA.setHostname(hostname);
    ArduinoOTA.setPassword(password);
}

void beginOTA()
{
    ArduinoOTA.setRebootOnSuccess(true);

    ArduinoOTA.onStart([]() {
        isOTAUpdating = true;
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        int OTAProgress = (progress / (total / 100));
        if(otaProgressHandler) 
        {
            otaProgressHandler(OTAProgress);
        }
    });

    ArduinoOTA.onEnd([]() {
        isOTAUpdating = false;
    });

    ArduinoOTA.onError([](ota_error_t error) {
        if (error == OTA_AUTH_ERROR)
            OTAError = String("Auth Failed");
        else if (error == OTA_BEGIN_ERROR)
            OTAError = String("Begin Failed");
        else if (error == OTA_CONNECT_ERROR)
            OTAError = String("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR)
            OTAError = String("Receive Failed");
        else if (error == OTA_END_ERROR)
            OTAError = String("End Failed");
    });

    ArduinoOTA.begin();
    beganOTA = true;
}

void updateOTA()
{
    if (!beganOTA)
        return;
    ArduinoOTA.handle();
}

#endif