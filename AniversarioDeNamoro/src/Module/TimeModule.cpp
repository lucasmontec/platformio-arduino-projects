#include "Module/TimeModule.h"

TimeModule::TimeModule(int utcOffsetHours)
{
    ntpUDP = new WiFiUDP();
    timeClient = new NTPClient(*ntpUDP, "pool.ntp.org", utcOffsetHours * 60 * 60);
}

void TimeModule::begin()
{
    timeClient->begin();
    began = true;
}

void TimeModule::update()
{
    if (!began)
    {
        if (millis() % 1000 <= 500)
        {
            timeStr = "00 00";
        }
        else
        {
            timeStr = "00:00";
        }
        return;
    }

    if (timeClient->update())
    {
        int minutes = timeClient->getMinutes();
        int hours = timeClient->getHours();

        if (millis() % 1000 <= 500)
        {
            timeStr = (hours < 10 ? "0" : "") + String(hours) + ":" + (minutes < 10 ? "0" : "") + String(minutes);
        }
        else
        {
            timeStr = (hours < 10 ? "0" : "") + String(hours) + " " + (minutes < 10 ? "0" : "") + String(minutes);
        }
    }
}

void TimeModule::render(UIBar *bar)
{
    bar->renderString(FROM_RIGHT, &timeStr);
}