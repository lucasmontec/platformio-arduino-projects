#ifndef TopBarModule_h
#define TopBarModule_h

#include "UIBar.h"

class TopBarModule
{
public:
    TopBarModule(){}
    virtual void begin();
    virtual void update();
    virtual void render(UIBar *bar);
};

#endif