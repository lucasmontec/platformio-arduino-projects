#ifndef UIBar_h
#define UIBar_h

#include "Arduino.h"
#include "SSD1306Wire.h"

//TOP BAR ICONS ARE 16x16

enum RenderDirection { FROM_LEFT, FROM_RIGHT };

class UIBar
{
  public:
    UIBar(SSD1306Wire *display, int start_x, int end_x, int y, int element_spacing);
    void renderIcon10x10(RenderDirection direction, const unsigned char* icon);
    void renderString(RenderDirection direction, String *value);
    void renderString(RenderDirection direction, const char *value, int length);
    void pad(int space, RenderDirection direction);
    OLEDDISPLAY_TEXT_ALIGNMENT getAlignmentForDirection(RenderDirection direction);
    void reset();
  private:
    SSD1306Wire *display;
    int current_x_from_left = 0;
    int current_x_from_right = 0;
    int start_x, end_x, y;
    int element_spacing = 2;
    int getStartAndIncrement(int width, RenderDirection direction);
};

#endif
