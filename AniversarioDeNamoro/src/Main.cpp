#include <Arduino.h>
#include <Wire.h>
#include "SSD1306Wire.h"

#include <ArduinoJson.h>

#include "OTA.h"

#include "FrameBase/FrameManager.h"
#include "Frame/MainFrame.h"
#include "Frame/MultipleTextFrame.h"
#include "Frame/QuestionFrame.h"

#include "Input.h"
#include "UIBar.h"
#include "WifiManager.h"
#include "BoundedCursor.h"

#include "UI/TextButton.h"
#include "UI/Button.h"

#include "Module/TopBarModule.h"
#include "Module/TimeModule.h"

#define MAX_MODULES 10

//XBM https://xbm.jazzychad.net/
//Display: https://github.com/ThingPulse/esp8266-oled-ssd1306
//OTA: https://www.youtube.com/watch?v=1pwqS_NUG7Q

//Display
SSD1306Wire display(0x3c, D1, D2);

FrameManager* frameManager;
UIBar* topBar;
WifiManager* wifiManager;
Frame* mainFrame;

TimeModule* timeModule = new TimeModule(-3);
TopBarModule* modules[MAX_MODULES] = {timeModule};

Frame* frames[3];

void onWifiConnected()
{
  timeModule->begin();
}

void otaProgress(unsigned int progress);

int currentPuzzle = 2;

void nextPuzzle()
{
  currentPuzzle++;
  frameManager->setFrame(frames[currentPuzzle]);
}

void openPuzzle()
{
  currentPuzzle = 2;
  frameManager->setFrame(frames[2]);
}

void openText()
{
  frameManager->setFrame(frames[1]);
}

void openMain()
{
  frameManager->setFrame(frames[0]);
}

void nextText()
{
  ((MultipleTextFrame*)frames[1])->nextText();
}

void nextText2()
{
  ((MultipleTextFrame*)frames[12])->nextText();
}

const char* texts[] = {
  "Oi gatinha! Tudo bem?",
  "Nao deu tempo...",
  "de fazer um robo",
  "complicado! Entao eu",
  "modifiquei o mais...",
  "complicado que eu ja",
  "fiz. Pra convidar voce",
  "a construir. Construir?",
  "Construir comigo! Tudo!",
  "mais robos, artes...",
  "mas alem disso tudo...",
  "UMA VIDA <3",
  "Entao criei um desafio",
  "nesse robo... tente vencer"
};

const char* texts2[] = {
  "pois é, voce venceu",
  "mas no fundo quem",
  "ganhou somos nos",
  "XABLAU",
  "agora foco em mais",
  "um ano de namoro!",
  "e viagens",
  "e fantasia",
  "e invencoes",
  "bjs do robo cansado",
  "fim",
};

void setup()
{
  setupInput();

  display.init();
  display.flipScreenVertically();

  topBar = new UIBar(&display, 0, 127, 0, 2);

  wifiManager = new WifiManager(String("Fidalga"), String("odinteabencoacomwifi"));

  wifiManager->setOnConnected(&onWifiConnected);

  frameManager = new FrameManager(new BoundedCursor(0, 12, 127, 63));

  Frame* main = new MainFrame(topBar);

  Frame* textFrame1 = new MultipleTextFrame(topBar, texts, 14, &openPuzzle);
  textFrame1->addButton(new TextButton(40, 45, String("Proximo!"), &nextText));

  //UIBar *bar, const char* text, void (*correctAction)(void), void (*wrongAction)(void), const char* correctText="sim", const char* wrongText="nao"
  Frame* challenge1 = new QuestionFrame(topBar, "dia comecamos a namorar?", &nextPuzzle, &openPuzzle, "16", "14");
  Frame* challenge2 = new QuestionFrame(topBar, "melhor alimento?", &nextPuzzle, &openPuzzle, "queijo", "sardinha");
  Frame* challenge3 = new QuestionFrame(topBar, "caminho da luz?", &openPuzzle, &nextPuzzle, "cidade", "natureza");
  Frame* challenge4 = new QuestionFrame(topBar, "amor do lucas?", &nextPuzzle, &openPuzzle, "thais", "juracema");
  Frame* challenge5 = new QuestionFrame(topBar, "animal do lucas?", &openPuzzle, &nextPuzzle , "cobra", "urso");
  Frame* challenge6 = new QuestionFrame(topBar, "posicao na cama?", &openPuzzle, &nextPuzzle , "de 4", "cowgirl");
  Frame* challenge7 = new QuestionFrame(topBar, "objetivo na vida?",  &nextPuzzle, &openPuzzle , "amar", "vencer");
  Frame* challenge8 = new QuestionFrame(topBar, "77 + 3?",  &nextPuzzle, &openPuzzle , "oi tenta", "80");
  Frame* challenge9 = new QuestionFrame(topBar, "nao tem como errar",  &nextPuzzle, &nextPuzzle , "certo", "sim");
  Frame* challenge10 = new QuestionFrame(topBar, "thais é?",  &nextPuzzle, &openPuzzle , "+formiga", "só gata");

  frames[0] = main;
  frames[1] = textFrame1;

  frames[2] = challenge1;
  frames[3] = challenge2;
  frames[4] = challenge3;
  frames[5] = challenge4;
  frames[6] = challenge5;
  frames[7] = challenge6;
  frames[8] = challenge7;
  frames[9] = challenge8;
  frames[10] = challenge9;
  frames[11] = challenge10;

  Frame* textFrame2 = new MultipleTextFrame(topBar, texts2, 11, &openMain);
  textFrame2->addButton(new TextButton(40, 45, String("Proximo..."), &nextText2));

  frames[12] = textFrame2;

  main->addButton(new TextButton(40, 25, String("Comecar!"), &openText));

  frameManager->setFrame(main);

  setupOTA("love-device", "8568");
  otaProgressHandler = otaProgress;
  beginOTA();
}

void frameLoop();
bool otaLoop();

void loop()
{
  updateOTA();

  if(isOTAUpdating) return;
  frameLoop();
}

void otaProgress(unsigned int progress)
{
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(63, 31-10-2-4, "OTA Update"); //31[center_vert], 10 = (5[bar_h/2] + 5[text_h/2]), 2[space]
  display.drawProgressBar(10, 32-5, 127-20, 10, progress);
  display.display();
}

void updateModules()
{
  for(int i=0;i<MAX_MODULES;i++)
  {
    TopBarModule* module = modules[i];
    if(module == NULL) continue;
    module->update();
    module->render(topBar);
  }
}

void frameLoop()
{
  display.clear();

  processInput(frameManager);

  updateModules();

  wifiManager->keepConnection();
  wifiManager->renderStatusIcon(topBar);

  frameManager->render(&display);

  display.display();
  topBar->reset();
}
