#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiManager.h>

//Matrix libs
#include <MD_Parola.h>
#include <MD_MAX72xx.h>

#include "controlPage.h"
#include "parolaExtensions.h"

#ifndef STASSID
#define STASSID "Casa dos Ursos"
#define STAPSK "0sUrsosTeAbencoamComWiFi"
#endif

#define MAX_MESSAGES_QUEUE 10
#define MAX_CHARS_ON_SCREEN 4

const char *ssid = STASSID;
const char *password = STAPSK;

ESP8266WebServer server(80);

IPAddress ip;

char currentMsg[512];
char nextMsg[512];

int currentSpeed = 80;
int nextSpeed;
bool hasNextSpeed;

int pause = 2000;

textEffect_t customEffectIn;
textEffect_t customEffectOut;
bool hasInEffect;
bool hasOutEffect;

bool presentingVoltage;
bool presentingBattery;

bool loopDisplay = true;

String messageQueue[MAX_MESSAGES_QUEUE];
int currentMessageQueueIndex = 0;
int messagesInQueue;

//Set voltage read to read 3.3v line
ADC_MODE(ADC_VCC);

//TYPE, DATA, CLK, CLS, DEVICES
MD_Parola parolaDisplay = MD_Parola(MD_MAX72XX::FC16_HW, D1, D3, D2, 3);

// >>>>>> PROTOTYPES
void updateDisplayBuffer(bool clearDisp);
void advanceCurrentMessage();
void voltagePresent();
void batteryPresent();

String ipToString(IPAddress ip)
{
  return String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
}

void serverMessageHandler()
{
  //Reset commands
  presentingVoltage = false;
  presentingBattery = false;

  if (server.arg("msg") == "")
  {
    server.send(200, "text/plain", "No message, send msg arg!");
    return;
  }

  parseEffectParameter(server.arg("out"), customEffectOut, hasOutEffect);
  parseEffectParameter(server.arg("in"), customEffectIn, hasInEffect);

  if (server.arg("pause") != "")
  {
    pause = server.arg("pause").toInt();
  }

  if (server.arg("loop") != "")
  {
    loopDisplay = server.arg("loop") == "true";
  }
  else
  {
    loopDisplay = false;
  }

  if (server.arg("delay") != "")
  {
    nextSpeed = server.arg("delay").toInt();
    if (nextSpeed != 0)
    {
      hasNextSpeed = true;
    }
  }

  String message = server.arg("msg");

  if (message.startsWith("$"))
  {

    //Voltage command
    if (message.substring(1) == "voltage")
    {
      presentingVoltage = true;
      voltagePresent();
      server.send(200, "text/plain", "Present voltage!");
      updateDisplayBuffer(true);
      return;
    }

    //Battery command
    if (message.substring(1) == "battery")
    {
      presentingBattery = true;
      batteryPresent();
      server.send(200, "text/plain", "Present battery level!");
      updateDisplayBuffer(true);
      return;
    }

    //IP command
    if (message.substring(1) == "ip")
    {
      strcpy(nextMsg, ipToString(ip).c_str());
      server.send(200, "text/plain", "Present ip!");
      updateDisplayBuffer(true);
      return;
    }
  }

  if (customEffectOut != PA_SCROLL_LEFT && customEffectOut != PA_SCROLL_RIGHT &&
      customEffectIn != PA_SCROLL_LEFT && customEffectIn != PA_SCROLL_RIGHT)
  {
    messagesInQueue = min((int)ceil((float)message.length() / MAX_CHARS_ON_SCREEN), MAX_MESSAGES_QUEUE);

    for (int i = 0; i < messagesInQueue; i++)
    {
      int startIndex = i * MAX_CHARS_ON_SCREEN;
      int endIndex = min((int)message.length(), startIndex + MAX_CHARS_ON_SCREEN);
      messageQueue[i] = message.substring(startIndex, endIndex);
    }
  }
  else
  {
    messagesInQueue = 1;
    messageQueue[0] = message;
  }

  currentMessageQueueIndex = 0;
  advanceCurrentMessage();

  updateDisplayBuffer(true);

  server.sendHeader("Location", String("/"), true);
  server.send(302, "text/plain", "queued: " + message);
}

float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void batteryPresent()
{
  float vcc = (float)ESP.getVcc() / 1023.0f;
  float perc = mapf(vcc, 3.6f, 4.2f, 0.0f, 100.0f);
  String finalPercent = String((int)perc) + "%";
  strcpy(nextMsg, finalPercent.c_str());
}

void voltagePresent()
{
  float vcc = (float)ESP.getVcc() / 1023.0f;
  String finalVoltage = String(vcc) + "V";
  strcpy(nextMsg, finalVoltage.c_str());
}

void handleNotFound()
{
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++)
  {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void showMessagePage()
{
  server.send(200, "html", messagePage);
}

void connectWifi()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100); // IMPORTANT, THIS RESETS WATCH DOG (idle detector)
  }
}

void connectWiFiManager()
{
  WiFiManager wifiManager;
  bool result = wifiManager.autoConnect("ChatBubbleAP");
  if(!result)
  {
    Serial.println("Failed WiFi Manager connect!");
  }
}

void setup()
{
  //Serial.begin(115200);

  parolaDisplay.begin();

  connectWiFiManager();

  MDNS.begin("chat-bubble"); //returns true if worked

  ip = WiFi.localIP();
  //Serial.println(ipToString(ip));

  strcpy(nextMsg, ipToString(ip).c_str());
  strcpy(currentMsg, nextMsg);

  parolaDisplay.displayScroll(currentMsg, PA_LEFT, PA_SCROLL_LEFT, 120);

  server.on("/", showMessagePage);
  server.on("/message", serverMessageHandler);

  server.onNotFound(handleNotFound);

  server.begin();
}

void loop()
{
  server.handleClient();
  MDNS.update();

  if (parolaDisplay.displayAnimate()) // If finished displaying message
  {
    advanceCurrentMessage();
    updateDisplayBuffer(false);

    if (loopDisplay)
    {
      parolaDisplay.displayReset(); // Reset and display it again
    }
  }
}

void advanceCurrentMessage()
{
  if (messagesInQueue == 0)
    return;

  String message = messageQueue[currentMessageQueueIndex];
  strcpy(nextMsg, message.c_str());

  currentMessageQueueIndex++;

  if (currentMessageQueueIndex >= messagesInQueue)
  {
    currentMessageQueueIndex = 0;
  }
}

void updateDisplayBuffer(bool clearDisp)
{
  if (clearDisp)
  {
    parolaDisplay.displayClear();
  }

  if (presentingVoltage)
  {
    voltagePresent();
  }

  strcpy(currentMsg, nextMsg);

  if (!hasOutEffect && !hasInEffect)
  {
    parolaDisplay.displayScroll(currentMsg, PA_LEFT, PA_SCROLL_LEFT, currentSpeed);
  }
  else
  {
    textEffect_t inEff = PA_NO_EFFECT;
    textEffect_t outEff = PA_NO_EFFECT;

    if (hasInEffect)
    {
      inEff = customEffectIn;
      outEff = customEffectIn;
    }

    if (hasOutEffect)
    {
      outEff = customEffectOut;
    }

    parolaDisplay.displayText(currentMsg, PA_CENTER, currentSpeed, pause, inEff, outEff);
  }

  if (hasNextSpeed)
  {
    currentSpeed = nextSpeed;
    parolaDisplay.setSpeed(nextSpeed);
    hasNextSpeed = false;
  }
}
