#include <Arduino.h>
#include <FastLED.h>
#include <WiFiManager.h> 
#include <WebServer.h>

#define MILLI_AMPS         2000 // IMPORTANT: set the max milli-Amps of your power supply (4A = 4000mA)
#define FRAMES_PER_SECOND  60

#define NUM_LEDS 6
#define DATA_PIN 1

WebServer server(80);

CRGB leds[NUM_LEDS];
WiFiManager wifiManager;

void connectingBlink();
void connectedBlink();
void handleSet();
void handleRoot();

bool firstConnected;

void setup() 
{
  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setMaxPowerInVoltsAndMilliamps(5, MILLI_AMPS);

  connectingBlink();
  wifiManager.autoConnect("AP_LAB_SIGN");

  server.on("/", HTTP_GET, handleRoot);
  server.on("/set", HTTP_GET, handleSet);
  server.begin();

  firstConnected = true;
}

void loop() 
{
  server.handleClient();
  
  //FastLED.show();
  //FastLED.delay(1000 / FRAMES_PER_SECOND);
}

void blink(int interval, const CRGB color)
{
  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = color; 
  }
  FastLED.show();
  delay(interval);
  
  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = CRGB::Black; 
  }
  FastLED.show();
  delay(interval);
}

void connectingBlink()
{
  blink(500, CRGB::Blue);
}

void connectedBlink()
{
  blink(250, CRGB::Green);
  blink(250, CRGB::Green);
}

void handleRoot()
{
  String html = "<html><head>";
  html += "<style>";
  html += "body { text-align: center; }";
  html += "a { display: inline-block; margin: 10px; padding: 10px 20px; text-decoration: none; font-size: 18px; color: white; background-color: #007BFF; border-radius: 5px; }";
  html += "</style>";
  html += "</head><body>";
  html += "<h1>LED Control</h1>";
  html += "<p><a href=\"/set?led=0&color=FF0000\">Generic Warning</a></p>";
  html += "<p><a href=\"/set?led=1&color=FF0000\">No Entry</a></p>";
  html += "<p><a href=\"/set?led=2&color=FF0000\">Flammable Warning</a></p>";
  html += "<p><a href=\"/set?led=3&color=FF0000\">Mask Warning</a></p>";
  html += "<p><a href=\"/set?led=4&color=FF0000\">Laser Warning</a></p>";
  html += "<p><a href=\"/set?led=5&color=FF0000\">Generic Warning</a></p>";
  html += "</body></html>";

  server.send(200, "text/html", html);
}

CRGB hexToCRGB(String hex)
{
  uint32_t rgb = strtol(hex.c_str(), NULL, 16);
  return CRGB((rgb >> 16) & 0xFF, (rgb >> 8) & 0xFF, rgb & 0xFF);
}

void setLedColor(String led, String color)
{
  int ledNum = led.toInt();
  if (ledNum >= 0 && ledNum < NUM_LEDS)
  {
    // Set the specified LED to the specified color
    leds[ledNum] = hexToCRGB(color);
    FastLED.show();
  }
}

void handleSet()
{
  if (server.hasArg("led") && server.hasArg("color"))
  {
    setLedColor(server.arg("led"), server.arg("color"));
  }
  server.sendHeader("Location", "/", true);
  server.send(302, "text/plain", "OK");
}