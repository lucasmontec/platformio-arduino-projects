void parseEffectParameter(const String parameter, textEffect_t& effect, bool& hasEffect) {
  if(parameter == "") {
    hasEffect = false;
    return;
  }
  if(parameter == "no"){
    effect = PA_NO_EFFECT;
    hasEffect = true;
    return;
  }
  if(parameter == "scroll_l"){
    effect = PA_SCROLL_LEFT;
    hasEffect = true;
    return;
  }
  if(parameter == "scroll_r"){
    effect = PA_SCROLL_RIGHT;
    hasEffect = true;
    return;
  }
  if(parameter == "random"){
    effect = PA_RANDOM;
    hasEffect = true;
    return;
  }
  if(parameter == "wipe"){
    effect = PA_WIPE;
    hasEffect = true;
    return;
  }
  if(parameter == "growup"){
    effect = PA_GROW_UP;
    hasEffect = true;
    return;
  }
  if(parameter == "growdown"){
    effect = PA_GROW_DOWN;
    hasEffect = true;
    return;
  }
  if(parameter == "dissolve"){
    effect = PA_DISSOLVE;
    hasEffect = true;
    return;
  }
  if(parameter == "blinds"){
    effect = PA_BLINDS;
    hasEffect = true;
    return;
  }
  if(parameter == "print"){
    effect = PA_PRINT;
    hasEffect = true;
    return;
  }
  if(parameter == "slice"){
    effect = PA_SLICE;
    hasEffect = true;
    return;
  }
  if(parameter == "scanh"){
    effect = PA_SCAN_HORIZ;
    hasEffect = true;
    return;
  }
  if(parameter == "scanv"){
    effect = PA_SCAN_VERT;
    hasEffect = true;
    return;
  }

  effect = PA_SCROLL_LEFT;
  hasEffect = false;
}