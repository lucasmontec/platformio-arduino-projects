#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

//Matrix libs
#include <MD_Parola.h>
#include <MD_MAX72xx.h>

#include "controlPage.h"
#include "parolaExtensions.h"

#ifndef STASSID
#define STASSID "Fidalga"
#define STAPSK "odinteabencoacomwifi"
#endif

#define MAX_MESSAGES_QUEUE 10
#define MAX_BATTERY_SAMPLES 10

const char *ssid = STASSID;
const char *password = STAPSK;

ESP8266WebServer server(80);

IPAddress ip;

char currentMsg[512];
char nextMsg[512];
bool hasNext;

int currentDelay = 80;
int nextDelay;
bool hasNextDelay;

int pause = 2000;

textEffect_t customEffectIn;
textEffect_t customEffectOut;
bool hasInEffect;
bool hasOutEffect;

bool presentingVoltage;
bool presentingBattery;

bool loopDisplay = true;

String messageQueue[MAX_MESSAGES_QUEUE];
int currentMessageQueueIndex = 0;
int storedMessages;

int lowBatteryMessageCount;
const int showLowBatteryEveryXMessages = 4;

int batteryPercentSamplesCount;
int currentBatterySample = 0;
float batteryPercentSamples[MAX_BATTERY_SAMPLES];

//Set voltage read to read 3.3v line
ADC_MODE(ADC_VCC);

//TYPE, DATA, CLK, CLS, DEVICES
MD_Parola P = MD_Parola(MD_MAX72XX::FC16_HW, D1, D3, D2, 3);

void updateDisplayBuffer(bool clearDisp, bool forceScroll);
void updateDisplayBuffer(bool clearDisp);

String ipToString(IPAddress ip)
{
  return String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
}

float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float GetVCC()
{
  return 0.51 + ((float)ESP.getVcc() / 1023.0f);
}

float GetBatteryPercent()
{
  float percent = mapf(GetVCC(), 3.6f, 4.12f, 0.0f, 100.0f);
  batteryPercentSamples[currentBatterySample++] = percent;

  if(currentBatterySample >= MAX_BATTERY_SAMPLES){
    currentBatterySample = 0;
  }

  if(batteryPercentSamplesCount < MAX_BATTERY_SAMPLES){
    batteryPercentSamplesCount++;
  }

  float roundedPercent = 0;
  for(int i=0;i<batteryPercentSamplesCount;i++){
    roundedPercent += batteryPercentSamples[i];
  }
  roundedPercent /= batteryPercentSamplesCount;

  return roundedPercent;
}

void batteryPresent(String preMessage = "")
{
  float perc = GetBatteryPercent();
  String finalPercent = preMessage + String((int)perc) + "%";
  strcpy(nextMsg, finalPercent.c_str());
  //Serial.println("battery present nxt message "+String(nextMsg));
  hasNext = true;
}

void voltagePresent()
{
  float vcc = GetVCC();
  String finalVoltage = String(vcc) + "V";
  strcpy(nextMsg, finalVoltage.c_str());
  hasNext = true;
}

void advanceCurrentMessage()
{
  if(storedMessages <= 0) return;

  String message = messageQueue[currentMessageQueueIndex];
  strcpy(nextMsg, message.c_str());
  //Serial.println("nxt message "+String(nextMsg));
  currentMessageQueueIndex++;

  if (currentMessageQueueIndex >= storedMessages)
  {
    currentMessageQueueIndex = 0;
  }

  hasNext = true;
}

void printServerMsg()
{
  //Reset commands
  presentingVoltage = false;
  presentingBattery = false;

  //Reset custom settings
  loopDisplay = true;

  if (server.arg("msg") == "")
  {
    server.send(200, "text/plain", "No message, send msg arg!");
    return;
  }

  parseEffectParameter(server.arg("out"), customEffectOut, hasOutEffect);
  parseEffectParameter(server.arg("in"), customEffectIn, hasInEffect);

  if (server.arg("pause") != "")
  {
    pause = server.arg("pause").toInt();
  }

  if (server.arg("loop") != "")
  {
    loopDisplay = server.arg("loop") == "true";
  }

  if (server.arg("delay") != "")
  {
    nextDelay = server.arg("delay").toInt();
    if (nextDelay != 0)
    {
      hasNextDelay = true;
    }
  }

  String message = server.arg("msg");

  if (message.startsWith("$"))
  {
    storedMessages = 0;

    //Voltage command
    if (message.substring(1) == "voltage")
    {
      presentingVoltage = true;
      voltagePresent();
      server.sendHeader("Location", String("/"), true);
      server.send(302, "text/plain", "Present voltage!");
      updateDisplayBuffer(true);
      return;
    }

    //Battery command
    if (message.substring(1) == "battery")
    {
      presentingBattery = true;
      batteryPresent();
      server.sendHeader("Location", String("/"), true);
      server.send(302, "text/plain", "Present battery level!");
      updateDisplayBuffer(true);
      return;
    }

    //IP command
    if (message.substring(1) == "ip")
    {
      server.sendHeader("Location", String("/"), true);
      server.send(302, "text/plain", "Present ip!");
      strcpy(nextMsg, ipToString(ip).c_str());
      hasNext = true;
      updateDisplayBuffer(true);
      return;
    }
  }

  if (customEffectOut != PA_SCROLL_LEFT && customEffectOut != PA_SCROLL_RIGHT &&
      customEffectIn != PA_SCROLL_LEFT && customEffectIn != PA_SCROLL_RIGHT)
  {
    storedMessages = min(((int)message.length() / 5) + 1, MAX_MESSAGES_QUEUE);
    for (int i = 0; i < storedMessages; i++)
    {
      int startIndex = i * 5;
      int endIndex = min((int)message.length(), startIndex + 5);
      messageQueue[i] = message.substring(startIndex, endIndex);
    }
  }
  else
  {
    storedMessages = 1;
    messageQueue[0] = message;
  }

  currentMessageQueueIndex = 0;
  advanceCurrentMessage();

  hasNext = true;

  updateDisplayBuffer(true);

  server.sendHeader("Location", String("/"), true);
  server.send(302, "text/plain", "queued: " + message);
  //server.send(200, "text/plain", "queued: "+message);
}

void handleNotFound()
{
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++)
  {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void showMessagePage()
{
  server.send(200, "html", messagePage);
}

void connectWifi()
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100); // IMPORTANT, THIS RESETS WATCH DOG (idle detector)
  }
}

void setup()
{
  //Serial.begin(115200);

  P.begin();

  connectWifi();

  MDNS.begin("esp8266"); //returns true if worked

  ip = WiFi.localIP();
  //Serial.println(ipToString(ip));

  strcpy(currentMsg, ipToString(ip).c_str());

  P.displayScroll(currentMsg, PA_LEFT, PA_SCROLL_LEFT, 120);

  server.on("/", showMessagePage);
  server.on("/message", printServerMsg);

  server.onNotFound(handleNotFound);

  server.begin();
}

void updateDisplayBuffer(bool clearDisp)
{
  updateDisplayBuffer(clearDisp, false);
}

void updateDisplayBuffer(bool clearDisp, bool forceScroll)
{
  if (clearDisp)
  {
    P.displayClear();
  }

  if (hasNext)
  {
    strcpy(currentMsg, nextMsg);

    if (forceScroll || (!hasOutEffect && !hasInEffect))
    {
      P.displayScroll(currentMsg, PA_LEFT, PA_SCROLL_LEFT, currentDelay);
    }
    else
    {
      textEffect_t inEff;
      textEffect_t outEff;
      if (hasInEffect)
      {
        inEff = customEffectIn;
        outEff = customEffectIn;
      }
      if (hasOutEffect)
      {
        outEff = customEffectOut;
      }
      P.displayText(currentMsg, PA_CENTER, currentDelay, pause, inEff, outEff);
    }

    hasNext = false;

    if (hasNextDelay)
    {
      currentDelay = nextDelay;
      P.setSpeed(nextDelay);
      hasNextDelay = false;
    }
  }
}

bool checkLowBattery()
{
  float perc = GetBatteryPercent();
  //Serial.println(perc);
  if (perc < 20.0f)
  {
    lowBatteryMessageCount++;

    if (lowBatteryMessageCount >= showLowBatteryEveryXMessages)
    {
      lowBatteryMessageCount = 0;
      batteryPresent("Low Battery ");
      //Serial.println("Low battery: ");
      
      return true;
    }
  }
  else
  {
    lowBatteryMessageCount = 0;
    presentingBattery = false;
  }
  return false;
}

void loop()
{
  server.handleClient();
  MDNS.update();

  // If finished displaying message
  if (P.displayAnimate())
  {
    //Update voltage value
    if (presentingVoltage)
    {
      voltagePresent();
    }

    //Update battery value
    if (presentingBattery)
    {
      batteryPresent();
    }

    if (!presentingBattery && !presentingVoltage)
    {
      advanceCurrentMessage();
      //Serial.println(currentMessageQueueIndex);
    }

    bool lowBattery = checkLowBattery();

    updateDisplayBuffer(false, lowBattery);

    if (loopDisplay)
    {
      P.displayReset(); // Reset and display it again
    }
  }
}
