#include <Arduino.h>
#include <Wire.h> 
#include "Command.h"

#pragma once

class RequestCommand : public Command {
public:
    RequestCommand() : Command("req") {
        description = "Request a specific number of bytes from an I2C device";
        help = "Usage: req <address> <numBytes>";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        requestI2CBytes(parameters);
    }

private:
    void requestI2CBytes(const String& params) {
        // Extract address
        int spaceIndex = params.indexOf(' ');
        String addrStr = params.substring(0, spaceIndex);

        int address = strtol(params.c_str(), NULL, 16);

        // Extract numBytes
        int numBytes = params.substring(spaceIndex + 1).toInt();

        Wire.requestFrom(address, numBytes);

        Serial.print("Received: ");
        while (Wire.available()) {
            Serial.print(Wire.read(), HEX);

            if (Wire.available()) {
                Serial.print(", ");
            }
        }
        Serial.println();
    }
};

class RequestWordCommand : public Command {
public:
    RequestWordCommand() : Command("reqw") {
        description = "Request a word from an I2C device, use 'reverse' to swap byte order, use 'int' to print response as int";
        help = "Usage: reqw <address> [reverse] [int]";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        requestI2CWord(parameters);
    }

private:
    void requestI2CWord(const String& params) {
        // Extract address
        int spaceIndex = params.indexOf(' ');
        String addrStr = params.substring(0, spaceIndex);

        int address = strtol(params.c_str(), NULL, 16);

        bool reverse = params.indexOf("reverse") != -1;
        bool showInt = params.indexOf("int") != -1;

        // Request 2 bytes for a word
        Wire.requestFrom(address, 2);

        if (Wire.available() == 2) {
            byte firstByte = Wire.read();
            byte secondByte = Wire.read();

            if (reverse) {
                Serial.print("Received word (reversed): ");
                if (showInt) {
                    Serial.println(word(secondByte, firstByte));
                } else {
                    Serial.println(word(secondByte, firstByte), HEX);
                }
            } else {
                Serial.print("Received word: ");

                if (showInt) {
                    Serial.println(word(firstByte, secondByte));
                } else {
                    Serial.println(word(firstByte, secondByte), HEX);
                }
            }
        } else {
            Serial.println("Failed to read 2 bytes from device.");
        }
    }
};