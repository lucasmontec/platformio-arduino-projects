#include <Arduino.h>
#include <Wire.h> 
#include "Command.h"

#pragma once

class ScanCommand : public Command {
public:
    ScanCommand() : Command("scan") {
        description = "Scan for I2C devices";
        help = "Usage: scan";
    }

    void execute(const String& command) override {
        scanI2CDevices();
    }

private:
    void scanI2CDevices() {
        String foundAddresses = "";
        Serial.println("\nScanning for I2C devices...");

        for (int address = 1; address < 127; address++) {
            Wire.beginTransmission(address);
            byte error = Wire.endTransmission();
            if (error == 0) {
                if (foundAddresses.length() > 0) {
                    foundAddresses += ", ";
                }

                char formattedAddress[6]; // "0xXX" + null terminator
                sprintf(formattedAddress, "0x%02X", address);
                foundAddresses += formattedAddress;
            }
        }

        if (foundAddresses.length() == 0) {
            Serial.println("No I2C devices found.");
        } else {
            Serial.print("Found addresses: ");
            Serial.println(foundAddresses);
        }
        Serial.println("Scan complete.");
    }
};
