#include <Arduino.h>
#include <Wire.h>
#include "Commands.h"

#define MAX_DEVICES 128

#define MAX_ALIASES 5
String aliasNames[MAX_ALIASES];
String aliasCommands[MAX_ALIASES];
int aliasCount = 0;

void helpCommand(String command);
void printMenu();
void addAlias(String command);
void clearAliases();

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Serial.println("I2C Tool Ready");

  printMenu();
  Wire.begin(sdaPin, sclPin, i2cFreq);
}

void loop() {
  if (Serial.available() > 0) {
    String command = Serial.readStringUntil('\n');
    command.trim(); // Remove any whitespace

    for (int i = 0; i < aliasCount; i++) {
      if (command == aliasNames[i]) {
        command = aliasCommands[i];
        break;
      }
    }

    if (command.startsWith("help ")) {
      helpCommand(command);
      return;
    }

    if (command.startsWith("alias ")) {
      addAlias(command);
      return;
    }

    if (command.startsWith("clearAliases")) {
      clearAliases();
      return;
    }

    if (command.startsWith("menu")) {
      printMenu();
      return;
    }

    for (int i = 0; i < commandCount; i++) {
      if (commands[i]->matches(command)) {
        commands[i]->execute(command);
        return;
      }
    }

    Serial.println("Unknown command. Please use 'menu' to list commands.");
  }
}

void helpCommand(String command) {
  // Remove 'help ' part
  command.remove(0, 5);

  int firstSpace = command.indexOf(' ');
  String commandName = command.substring(0, firstSpace);
  for(int i=0; i<commandCount; i++) {
    Command* command = commands[i];
    if(command->getName() == commandName) {
      Serial.println(command->getHelp());
    }
  }
}

void printMenu() {
  Serial.println("Available commands:");
  for(int i=0; i<commandCount; i++) {
    Command* command = commands[i];
    Serial.println("'"+command->getName()+"' - "+command->getDescription());
  }
  Serial.println("'menu' - Print the menu again");
  Serial.println("'help' - Prints command usage. Usage: help <command>");
  Serial.println("'alias' - Adds an alias. Max aliases "+String(MAX_ALIASES)+". 'alias <aliasName> <command to alias>'");
  Serial.println("'clearAliases' - Resets all aliases.");
}

void clearAliases() {
  aliasCount = 0;
  Serial.println("Aliases reset.");
}

void addAlias(String command) {
  // Remove 'alias ' part
  command.remove(0, 6);

  int firstSpace = command.indexOf(' ');
  String aliasName = command.substring(0, firstSpace);
  String aliasCommand = command.substring(firstSpace + 1);

  if (aliasCount < MAX_ALIASES) {
    aliasNames[aliasCount] = aliasName;
    aliasCommands[aliasCount] = aliasCommand;
    aliasCount++;
    Serial.println("Alias added: " + aliasName + " for command " + aliasCommand);
  } else {
    Serial.println("Alias limit reached.");
  }
}