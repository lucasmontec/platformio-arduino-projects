#include <Arduino.h>

#pragma once

class Command {
protected:
    String name;
    String description;
    String help;

public:
    Command(const String& name) : name(name) {}

    virtual void execute(const String& command) = 0;

    bool matches(const String& command) {
        int spaceIndex = command.indexOf(' ');
        String firstWord;
        if (spaceIndex != -1) {
            firstWord = command.substring(0, spaceIndex);
        } else {
            firstWord = command;
        }
        return firstWord.equals(name);
    }

    String getParameters(const String& command) {
        String params = command.substring(name.length());
        params.trim();
        return params; 
    }

    String getName() {
        return name;
    }

    String getDescription() {
        return description;
    }

    String getHelp() {
        return help;
    }
};
