#include <Arduino.h>
#include <Wire.h> 
#include "Command.h"

#pragma once

int sdaPin = 6;
int sclPin = 5;
long i2cFreq = 250000;

class StatusCommand : public Command {
public:
    StatusCommand() : Command("status") {
        description = "Shows current pins and frequency.";
        help = "Usage: status";
    }

    void execute(const String& command) override {
        printStatus();
    }

private:
    void printStatus() {
        Serial.println("I2C bus SDA pin " + String(sdaPin) + ", SCL pin " + String(sclPin) + ", Frequency: " + String(i2cFreq));
    }
};

class InitCommand : public Command {
public:
    InitCommand() : Command("init") {
        description = "Re-init I2C bus with specified SDA, SCL pins, and frequency";
        help = "Usage: init <sdaPin> <sclPin> <freq>";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        reInit(parameters);
    }

private:
    void reInit(const String& params) {
        int firstSpace = params.indexOf(' ');
        int secondSpace = params.indexOf(' ', firstSpace + 1);

        // Extract SDA, SCL pins, and frequency from the command
        sdaPin = params.substring(0, firstSpace).toInt();
        sclPin = params.substring(firstSpace + 1, secondSpace).toInt();
        i2cFreq = params.substring(secondSpace + 1).toInt();

        // Re-initialize I2C bus with new settings
        Wire.begin(sdaPin, sclPin);
        Wire.setClock(i2cFreq);

        Serial.println("I2C bus re-initialized with SDA pin " + String(sdaPin) + ", SCL pin " + String(sclPin) + ", Frequency: " + String(i2cFreq));
    }
};

class SetPinsCommand : public Command {
public:
    SetPinsCommand() : Command("setpins") {
        description = "Change SDA and SCL pins";
        help = "Usage: setpins <sdaPin> <sclPin>";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        setPins(parameters);
    }

private:
    void setPins(const String& params) {
        int spaceIndex = params.indexOf(' ');

        // Extract SDA and SCL pins from the command
        sdaPin = params.substring(0, spaceIndex).toInt();
        sclPin = params.substring(spaceIndex + 1).toInt();

        // Re-initialize I2C bus with new pins but keep the current frequency
        Wire.begin(sdaPin, sclPin);
        Wire.setClock(i2cFreq);

        Serial.println("I2C pins set to SDA: " + String(sdaPin) + ", SCL: " + String(sclPin));
    }
};

class SetFreqCommand : public Command {
public:
    SetFreqCommand() : Command("setfreq") {
        description = "Change I2C frequency (250000 is 250MHZ)";
        help = "Usage: setfreq <freq>";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        setFreq(parameters);
    }

private:
    void setFreq(const String& freqStr) {
        int freq = freqStr.toInt();
        Wire.setClock(freq);
        Serial.println("I2C frequency set to " + String(freq));
    }
};