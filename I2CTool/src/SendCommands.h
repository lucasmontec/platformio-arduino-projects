#include <Arduino.h>
#include <Wire.h> 
#include "Command.h"

#pragma once

class SendCommand : public Command {
public:
    SendCommand() : Command("send") {
        description = "Send bytes to an I2C device";
        help = "Usage: send <address> <byte1> <byte2> ...";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        sendI2CBytes(parameters);
    }

private:
    void sendI2CBytes(const String& parameters) {
        // Extract the address
        int spaceIndex = parameters.indexOf(' ');
        String addrStr = parameters.substring(0, spaceIndex);
        int address = strtol(addrStr.c_str(), NULL, 16);

        String message = "Sending to " + addrStr + ":";

        Wire.beginTransmission(address);

        int currentIndex = spaceIndex + 1;
        while (currentIndex < parameters.length() && spaceIndex != -1) {
            spaceIndex = parameters.indexOf(' ', currentIndex);
            String byteStr = parameters.substring(currentIndex, spaceIndex == -1 ? parameters.length() : spaceIndex);
            byteStr.toUpperCase();
            int byteVal = strtol(byteStr.c_str(), NULL, 16);
            Wire.write(byteVal);

            message += " " + byteStr; // Add byte to the message

            currentIndex = spaceIndex + 1;
        }

        byte result = Wire.endTransmission();
        Serial.println(message);
        Serial.print("Send result: ");
        Serial.print(result);
        Serial.println(result == 0 ? " SUCCESS" : " FAIL");
    }
};

class SendAllCommand : public Command {
public:
    SendAllCommand() : Command("senda") {
        description = "Send bytes to all I2C addresses";
        help = "Usage: senda <byte1> <byte2> ...";
    }

    void execute(const String& command) override {
        String parameters = getParameters(command);
        sendI2CToAll(parameters);
    }

private:
    void sendI2CToAll(const String& bytesToSend) {
        bool allSentSuccessfully = true;

        for (int address = 0; address < 127; address++) {
            Wire.beginTransmission(address);

            int currentIndex = 0;
            int spaceIndex = 0;
            while (currentIndex < bytesToSend.length() && spaceIndex != -1) {
                spaceIndex = bytesToSend.indexOf(' ', currentIndex);
                String byteStr = bytesToSend.substring(currentIndex, spaceIndex == -1 ? bytesToSend.length() : spaceIndex);
                byteStr.toUpperCase();
                int byteVal = strtol(byteStr.c_str(), NULL, 16);
                Wire.write(byteVal);

                currentIndex = spaceIndex + 1;
            }

            byte result = Wire.endTransmission();
            // If the result indicates a failure
            if (result != 0) {
                allSentSuccessfully = false;
                Serial.print("Failed to send to 0x");
                if (address < 16) Serial.print("0");
                Serial.print(address, HEX);
                Serial.print(". Error code: ");
                Serial.println(result);
            }
        }

        if (allSentSuccessfully) {
            Serial.println("Successfully sent to all I2C addresses.");
        } else {
            Serial.println("Failed to send to one or more I2C addresses.");
        }
    }
};