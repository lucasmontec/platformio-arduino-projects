#include <Arduino.h>
#include "Command.h"
#include "ScanCommand.h"
#include "SendCommands.h"
#include "RequestCommands.h"
#include "I2CConfigCommands.h"

#pragma once

ScanCommand scanCommand;

//Send
SendCommand sendCommand;
SendAllCommand sendAllCommand;

//Request
RequestCommand requestCommand;
RequestWordCommand requestWordCommand;

//Config IIC
InitCommand reinitCommand;
SetPinsCommand setPinsCommand;
SetFreqCommand setFrequencyCommand;
StatusCommand statusCommand;

static Command* const commands[] = { 

    &scanCommand,

    &sendCommand,
    &sendAllCommand,

    &requestCommand,
    &requestWordCommand,

    &reinitCommand,
    &setPinsCommand,
    &setFrequencyCommand,
    &statusCommand

    };

static const int commandCount = sizeof(commands) / sizeof(commands[0]);