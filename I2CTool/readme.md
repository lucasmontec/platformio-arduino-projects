# I2C Serial Command Line Tool

## Overview

This I2C Tool is designed to facilitate interaction with I2C devices directly from the serial command line. It allows users to scan the I2C bus for devices, send bytes to specific addresses, send bytes to all addresses, request bytes from devices, and manipulate the I2C bus configuration. 

## Features

- **Scan the I2C bus** for connected devices.

- **Send bytes** to a specific device on the bus.

- **Broadcast bytes** to all devices on the bus.

- **Request bytes** or a **word** from a device, with optional byte order reversal.

- **Configure I2C** in runtime.

## Usage

### Basic Commands

- ***scan***: Scans the I2C bus for devices and lists found addresses.
- ***send <address> <byte1> <byte2> ...***: Sends bytes to a specific I2C address. Example: `send 0x68 0x00 0x01`.
- ***sendAll <byte1> <byte2> ...***: Sends bytes to all possible I2C addresses, reporting any failures. Example: `sendAll 0x00 0x01`.
- ***req <address> <numBytes>***: Requests a specified number of bytes from an I2C device. Example: `req 0x68 3`.
- ***reqw <address> [reverse] [int]***: Request a word from an I2C device, use 'reverse' to swap byte order, use 'int' to print response as int. Example: `reqw 0x68 reverse int`.

### Advanced Configuration Commands
- ***status***: Shows the current SDA and SCL pins and I2C bus frequency.
- ***init <sdaPin> <sclPin> <freq>***: Re-initializes the I2C bus with specified SDA, SCL pins, and frequency. Example: `init 21 22 400000`.
- ***setpins <sdaPin> <sclPin>***: Changes the SDA and SCL pins used by the I2C bus. Example: `setpins 21 22`.
- ***setfreq <freq>***: Changes the I2C bus frequency. Example: `setfreq 400000`.

### Special Case Commands

#### Alias Command
- ***alias <aliasName> <command>***: Allows you to create a custom alias for a sequence of commands. This is particularly useful for simplifying complex command sequences or frequently used commands. The alias then becomes an available command that executes the specified sequence. Example usage: `alias reqMeasurement send 0x70 0x56`. After setting this alias, typing `reqMeasurement` will execute `send 0x70 0x56`.

#### Clear Aliases Command
- ***clearAliases***: Removes all previously set aliases. This command is useful for clearing out all custom shortcuts you have created, returning the command set to its default state. No parameters needed. Simply type `clearAliases` to remove all aliases.

#### Help Command
- ***help <command>***: Provides detailed help about a specific command, including how to use it and any parameters it accepts. This is useful for learning more about what each command does and how to properly execute it. Example usage: `help send` would provide information on how to use the `send` command.

#### Menu Command
- ***menu***: Prints the main command menu, listing all available commands and their brief descriptions. This is useful for quickly reminding yourself of the tool's capabilities and syntax without referring back to external documentation. Example usage: `menu` to display the command list.

### Using Aliases
Aliases provide a powerful way to customize your interaction with the I2C tool, allowing for quicker and more efficient use of the tool's functionality. Create aliases for commands you use frequently, or for complex sequences you want to execute with a single command.

### Resetting Aliases
If you find that your list of aliases has become cluttered or you wish to start over with your command shortcuts, use the `clearAliases` command to wipe the slate clean.

## Contributing

Contributions to the project are welcome. Please ensure any pull requests or issues are clear and provide as much context as possible.

## License

You can use it. Im not liable for anything you do with it. By downloading this you agree to assume full responsibility for what you do with it.