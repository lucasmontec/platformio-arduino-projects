#include <Arduino.h>

#include <ESP8266WiFi.h>

#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#include "TinyUPnP.h"
#include "wifiInfo.h"
#include "pages.h"

TinyUPnP *tinyUPnP = new TinyUPnP(20000);

#define LISTEN_PORT 27011  // http://<IP or DDNS>:<LISTEN_PORT>/?percentage=<0..100>
#define LEASE_DURATION 36000  // seconds
#define FRIENDLY_NAME "OrionServerRelayController"  // this name will appear in your router port forwarding section

bool portMappingAdded;

const char* ssid = STASSID;
const char* password = STAPSK;

AsyncWebServer server(80);
IPAddress ip;

const char* http_username = "admin";
const char* http_password = "admin";

bool togglePower;
bool reset;

void connectWiFi()
{
  while (WiFi.status() != WL_CONNECTED) {
    delay(150); 
  }
  ip = WiFi.localIP();
}

void setup()
{
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);

  digitalWrite(D6, LOW);
  digitalWrite(D5, LOW);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  connectWiFi();

  // you may repeat 'addPortMappingConfig' more than once
  tinyUPnP->addPortMappingConfig(WiFi.localIP(), LISTEN_PORT, RULE_PROTOCOL_TCP, LEASE_DURATION, FRIENDLY_NAME);

  // finally, commit the port mappings to the IGD
  portMappingAdded = tinyUPnP->commitPortMappings();

  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();
    request->send(200, "text/html", index_html);
  });

  server.on("/power", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();

    togglePower = true;

    request->send(200, "text/plain", "OK");
  });

  server.on("/reset", HTTP_GET, [](AsyncWebServerRequest *request){
    if(!request->authenticate(http_username, http_password))
      return request->requestAuthentication();

    reset = true;

    request->send(200, "text/plain", "OK");
  });

  //server.onNotFound(handleNotFound);
  server.begin();
}

String ipToString(IPAddress ip){
  return String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
}

void loop()
{
  tinyUPnP->updatePortMappings(600000, &connectWiFi);  // 10 minutes
  MDNS.update();

  if(togglePower)
  {
    togglePower = false;
    digitalWrite(D6, HIGH);
    delay(2000);
    digitalWrite(D6, LOW);
  }

  if(reset)
  {
    reset = false;
    digitalWrite(D5, HIGH);
    delay(2000);
    digitalWrite(D5, LOW);
  }
}
