const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>Orion Power Controller</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    html {font-family: Arial; display: inline-block; text-align: center;}
    h2 {font-size: 2.6rem;}
    body {max-width: 600px; margin:0px auto; padding-bottom: 10px;}
  </style>
</head>
<body>
  <h2>Orion Power Controller</h2>
  <button type="button" onclick="powerToggle()">Power On</button>
  <button type="button" onclick="resetToggle()">Reset</button>

  <script>
    function powerToggle() {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/power", true);
      xhr.send();
    }

    function resetToggle() {
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/reset", true);
      xhr.send();
    }
  </script>
</body>
</html>
)rawliteral";