#ifndef BoundedCursor_h
#define BoundedCursor_h

#include "Arduino.h"
#include "SSD1306Wire.h"

class BoundedCursor
{
public:
    BoundedCursor(int start_x, int start_y, int end_x, int end_y, float speed = 0.4f, int size = 2)
        : start_x(start_x), start_y(start_y), end_x(end_x), end_y(end_y), speed(speed), size(size)
    {
        cursor_x = start_x + (end_x - start_x) / 2;
        cursor_y = start_y + (end_y - start_y) / 2;
    }

    void render(SSD1306Wire *display);
    void moveUp();
    void moveDown();
    void moveLeft();
    void moveRight();
    void setSelected();
    void setReleased();
    int getX();
    int getY();

private:
    int start_x = 0, start_y = 0;
    int end_x = 0, end_y = 0;
    float cursor_x, cursor_y;
    float speed;
    int size;
    bool selected = false;
    void keepBounds();
};

#endif