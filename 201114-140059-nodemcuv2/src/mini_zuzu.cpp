#include <Arduino.h>
#include <Wire.h>
#include "SSD1306Wire.h"

#include <ArduinoJson.h>

#include "OTA.h"

#include "Input.h"
#include "FrameManager.h"
#include "MainFrame.h"
#include "UIBar.h"
#include "WifiManager.h"
#include "TimeModule.h"
#include "BoundedCursor.h"

//XBM https://xbm.jazzychad.net/
//Display: https://github.com/ThingPulse/esp8266-oled-ssd1306
//OTA: https://www.youtube.com/watch?v=1pwqS_NUG7Q

//Display
SSD1306Wire display(0x3c, D1, D2);

FrameManager *frameManager;
UIBar *topBar;
WifiManager *wifiManager;
TimeModule *timeModule;
Frame *mainFrame;

void onWifiConnected()
{
  timeModule->begin();
}

void setup()
{
  setupInput();

  display.init();
  display.flipScreenVertically();

  topBar = new UIBar(&display, 0, 127, 0, 2);

  timeModule = new TimeModule(-3);
  wifiManager = new WifiManager(String("Fidalga"), String("odinteabencoacomwifi"));

  wifiManager->setOnConnected(&onWifiConnected);

  frameManager = new FrameManager(new BoundedCursor(0, 12, 127, 63));
  frameManager->setFrame(new MainFrame(topBar));

  setupOTA("minizuzu", "1234");
  beginOTA();
}

void frameLoop();
bool otaLoop();

void loop()
{
  if (otaLoop()) return;
  frameLoop();
}

bool otaLoop()
{
  updateOTA();

  if(isOTAUpdating)
  {
    display.clear();
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.drawString(63, 31-10-2, "OTA Update"); //31[center_vert], 10 = (5[bar_h/2] + 5[text_h/2]), 2[space]
    display.drawProgressBar(10, 32-5, 127-20, 10, OTAProgress);
    display.display();
  }

  return isOTAUpdating;
}

void frameLoop()
{
  display.clear();

  processInput(frameManager);

  timeModule->update();
  timeModule->renderTimeString(topBar);

  wifiManager->keepConnection();
  wifiManager->renderStatusIcon(topBar);

  frameManager->render(&display);

  display.display();
  topBar->reset();
}