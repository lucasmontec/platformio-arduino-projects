#ifndef TimeModule_h
#define TimeModule_h

#include <WiFiUdp.h>
#include <NTPClient.h>
#include "UIBar.h"

class TimeModule
{
public:
    TimeModule(int utcOffsetHours);
    void begin();
    void update();
    void renderTimeString(UIBar *bar);

private:
    WiFiUDP *ntpUDP;
    NTPClient *timeClient;
    long began;
    String timeStr = "00:00";
};

#endif