#ifndef WifiManager_h
#define WifiManager_h

#include <ESP8266WiFi.h>
#include "UIBar.h"
#include "icons.h"

class WifiManager
{
public:
  WifiManager(String ssid, String password) : ssid(ssid), password(password) {}
  void keepConnection();
  void renderStatusIcon(UIBar *bar);
  bool isConnected();
  void setOnConnected(void (*onConnected)(void));

private:
  String ssid;
  String password;
  bool connecting;
  bool connected;
  void (*onConnected)(void);
};

#endif
