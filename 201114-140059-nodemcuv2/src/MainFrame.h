#ifndef MainFrame_h
#define MainFrame_h

#include "Arduino.h"
#include "Frame.h"
#include "UIBar.h"

class MainFrame : public Frame
{
public:
  MainFrame(UIBar *bar);
  void render(SSD1306Wire *display) override;
};

#endif
