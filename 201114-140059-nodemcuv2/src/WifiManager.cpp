#include "WifiManager.h"

void WifiManager::keepConnection()
{
    if (WiFi.status() == WL_CONNECTED)
    {
        connecting = false;
        if (!connected)
        {
            if (onConnected != 0)
                onConnected();
            connected = true;
        }
        return;
    }

    if (!connecting)
    {
        WiFi.disconnect();
        WiFi.begin(ssid, password);
        connecting = true;
    }

    if (WiFi.status() != WL_CONNECTED)
    {
        connected = false;
        delay(20);
    }
}

void WifiManager::renderStatusIcon(UIBar *bar)
{
    if (connecting)
    {
        delay(20);

        int elapsed = millis() % 1000;
        if (elapsed < 300)
        {
            bar->renderIcon10x10(FROM_RIGHT, wifi_connecting_1_10_10);
        }
        else if (elapsed >= 300 && elapsed <= 600)
        {
            bar->renderIcon10x10(FROM_RIGHT, wifi_connecting_2_10_10);
        }
        else
        {
            bar->renderIcon10x10(FROM_RIGHT, wifi_connecting_3_10_10);
        }
    }
    else
    {
        bar->renderIcon10x10(FROM_RIGHT, wifi_on_10_10);
    }
}

bool WifiManager::isConnected()
{
    return WiFi.status() == WL_CONNECTED;
}

void WifiManager::setOnConnected(void (*onConnected)(void))
{
    this->onConnected = onConnected;
}