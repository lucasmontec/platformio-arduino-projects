#ifndef Input_h
#define Input_h

#include <Arduino.h>
#include "FrameManager.h"

void setupInput()
{
    pinMode(D4, INPUT_PULLUP); //up
    pinMode(D3, INPUT_PULLUP); //down
    pinMode(D7, INPUT_PULLUP); //left
    pinMode(D6, INPUT_PULLUP); //right
    pinMode(D5, INPUT_PULLUP); //select
}

void processInput(FrameManager *frameManager)
{
    frameManager->inputUp(digitalRead(D4) == LOW); //up
    frameManager->inputDown(digitalRead(D3) == LOW); //down
    frameManager->inputLeft(digitalRead(D7) == LOW); //left
    frameManager->inputRight(digitalRead(D6) == LOW); //right
    frameManager->inputSelect(digitalRead(D5) == LOW); //select
}

#endif