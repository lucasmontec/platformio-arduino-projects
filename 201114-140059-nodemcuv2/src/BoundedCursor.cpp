#include "BoundedCursor.h"

void BoundedCursor::render(SSD1306Wire *display)
{
    if (!selected)
    {
        int minX = cursor_x - size < start_x ? start_x : cursor_x - size;
        int maxX = cursor_x + size > end_x ? end_x : cursor_x + size;

        int minY = cursor_y - size < start_y ? start_y : cursor_y - size;
        int maxY = cursor_y + size > end_y ? end_y : cursor_y + size;

        display->drawLine(minX, cursor_y, maxX, cursor_y);
        display->drawLine(cursor_x, minY, cursor_x, maxY);
    }
    else
    {
        display->fillCircle(cursor_x, cursor_y, size);
    }
}

void BoundedCursor::moveUp()
{
    cursor_y -= speed;
    keepBounds();
}

void BoundedCursor::moveDown()
{
    cursor_y += speed;
    keepBounds();
}

void BoundedCursor::moveLeft()
{
    cursor_x -= speed;
    keepBounds();
}

void BoundedCursor::moveRight()
{
    cursor_x += speed;
    keepBounds();
}

int BoundedCursor::getX()
{
    return cursor_x;
}

int BoundedCursor::getY()
{
    return cursor_y;
}

void BoundedCursor::setSelected()
{
    selected = true;
}

void BoundedCursor::setReleased()
{
    selected = false;
}

void BoundedCursor::keepBounds()
{
    if (cursor_x < start_x)
    {
        cursor_x = start_x;
    }
    if (cursor_x > end_x)
    {
        cursor_x = end_x;
    }
    if (cursor_y < start_y)
    {
        cursor_y = start_y;
    }
    if (cursor_y > end_y)
    {
        cursor_y = end_y;
    }
}