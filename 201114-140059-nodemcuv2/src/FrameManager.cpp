#include "FrameManager.h"

FrameManager::FrameManager(BoundedCursor *cur)
{
    cursor = cur;
}

void FrameManager::setFrame(Frame *frame)
{
    if (currentFrame)
    {
        currentFrame->end();
    }

    currentFrame = frame;

    if (currentFrame)
    {
        currentFrame->begin();
    }
}

void FrameManager::render(SSD1306Wire *display)
{
    if (currentFrame)
    {
        currentFrame->render(display);
    }

    if (useCursor())
    {
        cursor->render(display);
    }
}

void FrameManager::inputUp(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputUp(val);
    }

    if (useCursor() && val)
    {
        cursor->moveUp();
    }
}

void FrameManager::inputDown(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputDown(val);
    }

    if (useCursor() && val)
    {
        cursor->moveDown();
    }
}

void FrameManager::inputLeft(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputLeft(val);
    }

    if (useCursor() && val)
    {
        cursor->moveLeft();
    }
}

void FrameManager::inputRight(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputRight(val);
    }

    if (useCursor() && val)
    {
        cursor->moveRight();
    }
}

void FrameManager::inputSelect(bool val)
{
    if (currentFrame)
    {
        currentFrame->onInputSelect(val);
    }

    if (useCursor())
    {
        if (val)
        {
            cursor->setSelected();
        }
        else
        {
            cursor->setReleased();
        }
    }
}

bool FrameManager::useCursor()
{
    return cursor && currentFrame && currentFrame->useCursor;
}