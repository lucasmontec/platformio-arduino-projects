#include "UIBar.h"

UIBar::UIBar(SSD1306Wire *display, int start_x, int end_x, int y, int element_spacing)
    : display(display), start_x(start_x), end_x(end_x), y(y), element_spacing(element_spacing)
{
}

void UIBar::renderIcon10x10(RenderDirection direction, const unsigned char *icon)
{
  int start_point = getStartAndIncrement(10, direction);

  if (direction == FROM_RIGHT)
  {
    start_point -= 10;
  }

  display->drawXbm(start_point, y, 10, 10, icon);
}

void UIBar::renderString(RenderDirection direction, String *value)
{
  display->setFont(ArialMT_Plain_10);

  int width = display->getStringWidth(*value);
  int start_point = getStartAndIncrement(width, direction);

  display->setTextAlignment(getAlignmentForDirection(direction));

  display->drawString(start_point, y, *value);
}

void UIBar::renderString(RenderDirection direction, const char *value, int length)
{
  display->setFont(ArialMT_Plain_10);

  int width = display->getStringWidth(value, length);
  int start_point = getStartAndIncrement(width, direction);

  display->setTextAlignment(getAlignmentForDirection(direction));

  display->drawString(start_point, y, value);
}

OLEDDISPLAY_TEXT_ALIGNMENT UIBar::getAlignmentForDirection(RenderDirection direction)
{
  switch (direction)
  {
  case FROM_LEFT:
    return TEXT_ALIGN_LEFT;
  case FROM_RIGHT:
    return TEXT_ALIGN_RIGHT;
  default:
    return TEXT_ALIGN_LEFT;
  }
}

int UIBar::getStartAndIncrement(int width, RenderDirection direction)
{
  int start_point = 0;

  switch (direction)
  {
  case FROM_LEFT:
    start_point = current_x_from_left;
    current_x_from_left += width + element_spacing;
    break;
  case FROM_RIGHT:
    start_point = current_x_from_right;
    current_x_from_right -= width + element_spacing;
    break;
  default:
    break;
  }

  return start_point;
}

void UIBar::pad(int space, RenderDirection direction)
{
  switch (direction)
  {
  case FROM_LEFT:
    current_x_from_left += space;
    break;
  case FROM_RIGHT:
    current_x_from_right -= space;
    break;
  default:
    break;
  }
}

void UIBar::reset()
{
  current_x_from_left = start_x;
  current_x_from_right = end_x;
}
