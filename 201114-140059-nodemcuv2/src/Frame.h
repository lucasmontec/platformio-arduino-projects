#ifndef Frame_h
#define Frame_h

#include "Arduino.h"
#include "UIBar.h"

class Frame
{
public:
  bool useCursor = true;

  Frame(UIBar *bar) : bar(bar) {}
  virtual void render(SSD1306Wire *display) = 0;
  virtual void begin() {}
  virtual void end() {}

  virtual void onInputUp(bool val) { inputUp = val; }
  virtual void onInputDown(bool val) { inputDown = val; }
  virtual void onInputLeft(bool val) { inputLeft = val; }
  virtual void onInputRight(bool val) { inputRight = val; }
  virtual void onInputSelect(bool val) { inputSelect = val; }

protected:
  UIBar *bar;

  bool inputUp;
  bool inputDown;
  bool inputLeft;
  bool inputRight;
  bool inputSelect;
};

#endif
