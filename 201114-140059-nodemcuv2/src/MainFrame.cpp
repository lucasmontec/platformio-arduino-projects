#include "MainFrame.h"

MainFrame::MainFrame(UIBar *bar) : Frame(bar)
{
}

void MainFrame::render(SSD1306Wire *display)
{
    bar->renderString(FROM_LEFT, "Mini-ZUZU", 8);
    display->setTextAlignment(TEXT_ALIGN_CENTER);
    
    /*
    if (inputUp)
    {
        display->drawString(63, 35, "UP");
    }

    if (inputDown)
    {
        display->drawString(63, 35, "DOWN");
    }

    if (inputLeft)
    {
        display->drawString(63, 35, "LEFT");
    }

    if (inputRight)
    {
        display->drawString(63, 35, "RIGHT");
    }

    if (inputSelect)
    {
        display->fillCircle(63, 35, 3);
    }
    */
}