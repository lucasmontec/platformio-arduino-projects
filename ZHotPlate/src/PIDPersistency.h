#include "PID.h"
#include <Preferences.h>

#pragma once

Preferences preferences;
const char* preferenceSpace = "pidSettings";

const int KP_ADDRESS = 0;
const int KI_ADDRESS = 4;
const int KD_ADDRESS = 8;

void savePIDSettings()
{
    PIDController& controller = PIDController::getInstance();

    float Kp, Ki, Kd;
    controller.getPID(Kp, Ki, Kd);

    preferences.begin(preferenceSpace, false);
    preferences.putFloat("Kp", Kp);
    preferences.putFloat("Ki", Ki);
    preferences.putFloat("Kd", Kd);
    preferences.end();
}

void loadPIDSettings() 
{
    preferences.begin(preferenceSpace, false);

    float Kp = preferences.getFloat("Kp", 0.0); // Default to 0.0 if not set
    float Ki = preferences.getFloat("Ki", 0.0); // Default to 0.0 if not set
    float Kd = preferences.getFloat("Kd", 0.0); // Default to 0.0 if not set

    preferences.end();

    PIDController& controller = PIDController::getInstance();
    controller.setPID(Kp, Ki, Kd);
}