#pragma once
#include <Arduino.h>
#include <ADS1X15.h>

class ThermistorSensor {
private:
    ADS1115 ADS;

    // https://en.wikipedia.org/wiki/Thermistor
    const float R = 100000;  // Fixed resistor value (100k ohms)
    const float R0 = 100000;  // Resistance of thermistor at 25°C
    const float T0 = 298.15;  // Reference temperature in Kelvin (25°C)
    const float B = 3950;  // Beta coefficient
    
    float temperature_accumulator = 0;
    float last_average = 0;
    int current_sample = 0;
    unsigned long nextSample = 0;
    unsigned long sampleInterval = 5;  // in milliseconds
    int sampleCount = 6;  // number of samples to average

public:
    ThermistorSensor(int sampleCount = 6, unsigned long sampleInterval = 5) 
        : ADS(0x48), sampleCount(sampleCount), sampleInterval(sampleInterval) {}

    bool begin() 
    {
        bool ret = ADS.begin();
        ADS.setGain(0);
        return ret;
    }

    void update() 
    {
        if (millis() < nextSample) 
        {
            return;
        }
        nextSample = millis() + sampleInterval;

        int16_t adc0 = ADS.readADC(0);
        float adcVoltageFactor = ADS.toVoltage(1);
        float adc0Voltage = adc0 * adcVoltageFactor;

        float Rthermistor = R * (3.3 / adc0Voltage - 1); 
        float temperatureKelvin = 1.0 / (1.0 / T0 + 1.0 / B * log(Rthermistor / R0));

        temperature_accumulator += temperatureKelvin;
        current_sample++;

        if (current_sample >= sampleCount) 
        {
            last_average = temperature_accumulator / sampleCount;
            current_sample = 0;
            temperature_accumulator = 0;
        }
    }

    float temperatureKelvin() const 
    {
        return last_average;
    }

    float temperatureCelsius() const 
    {
        return KtoC(last_average);
    }

    int hasError()
    {
        return ADS.getError() != ADS1X15_OK;
    }
private:

    float KtoC(const float k) const
    {
        return k - 273.15f;
    }
};
