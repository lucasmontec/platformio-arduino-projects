#include <Arduino.h>

#pragma once

#define DEADZONE 3

class PIDController 
{
private:
    float Kp;
    float Ki;
    float Kd;

    float setPoint = 0;

    float integral = 0;
    float previous_error = 0;
    unsigned long lastTime = 0;

    float maxPWMOut = 255;

    const float MAX_TEMPERATURE_C = 500.0;

    PIDController() : Kp(1.0), Ki(0.1), Kd(0.05) {}

public:
    static PIDController& getInstance() 
    {
        static PIDController instance;
        return instance;
    }

    PIDController(PIDController const&) = delete; // Prevent copying
    void operator=(PIDController const&) = delete; // Prevent assignment

    void setMaxPWMOut(byte max)
    {
        maxPWMOut = (float)max;
    }

    float getTarget()
    {
        return setPoint;
    }

    void setTarget(float sp) 
    {
        setPoint = min(sp, MAX_TEMPERATURE_C);
        begin();
    }

    void setD(float d) 
    {
        Kd = d;
    }

    void setI(float i) 
    {
        Ki = i;
    }

    void setP(float p) 
    {
        Kp = p;
    }

    void setPID(float p, float i, float d) 
    {
        Kp = p;
        Ki = i;
        Kd = d;
    }

    void getPID(float &p, float &i, float &d) 
    {
        p = Kp;
        i = Ki;
        d = Kd;
    }

    void begin() 
    {
        integral = 0;
        previous_error = 0;
        lastTime = millis();
    }

    int calculatePID(const float currentTemp) 
    {
        unsigned long now = millis();
        float timeChange = (float)(now - lastTime);
        float error = setPoint - currentTemp;

        // Proportional term
        float Pout = Kp * error;

        // Integral term setup (to be calculated later based on conditions)
        float Iout = 0;

        // Derivative term
        float derivative = (error - previous_error) / timeChange;
        float Dout = Kd * derivative;

        // Calculate preliminary output without integral part to check conditions
        float output = Pout + Dout;

        // Limiter switch to prevent integral windup
        bool canAccumulateIntegral = true;

        // Check if output is already at control limits
        if ((output >= maxPWMOut && error > DEADZONE) || (output <= 0 && error < -DEADZONE)) 
        {
            canAccumulateIntegral = false;
            integral = 0;  // Optional: Reset integral when fully saturated
        }

        // Accumulate integral if limiter is true
        if (canAccumulateIntegral)
        {
            integral += error * timeChange;
            Iout = Ki * integral;
        }

        // Add the integral output
        output += Iout;

        // Constrain output to PWM limits
        output = constrain(output, 0, maxPWMOut);

        // Save current error and time for next calculation
        previous_error = error;
        lastTime = now;

        // Return PWM output
        return (int)output;
    }

/*
    int calculatePID(const float currentTemp) 
    {
        unsigned long now = millis();
        float timeChange = (float)(now - lastTime);

        float error = setPoint - currentTemp;

        // Proportional term
        float Pout = Kp * error;

        // Integral term
        if (currentTemp < MAX_TEMPERATURE_C) 
        {
            integral += error * timeChange;
        } 
        else 
        {
            // Reset integral on maximum temperature
            integral = 0; 
        }
        float Iout = Ki * integral;

        // Derivative term
        float derivative = (error - previous_error) / timeChange;
        float Dout = Kd * derivative;

        // Calculate total output
        float output = Pout + Iout + Dout;

        // Constrain output to PWM limits
        output = constrain(output, 0, 255);

        // Save current error and time for next calculation
        previous_error = error;
        lastTime = now;

        // Return PWM output
        return (int)output;
    }
*/
};