#pragma once

#include <Arduino.h>
#include "PID.h"
#include "PIDPersistency.h"

class SerialPIDCommandHandler 
{
private:
    SerialPIDCommandHandler() {}  // Private constructor for singleton

public:
    static SerialPIDCommandHandler& getInstance() 
    {
        static SerialPIDCommandHandler instance;
        return instance;
    }

    SerialPIDCommandHandler(SerialPIDCommandHandler const&) = delete;
    void operator=(SerialPIDCommandHandler const&) = delete;

    void processCommands() 
    {
        if (Serial.available() <= 0) 
        {
            return;
        }

        String command = Serial.readStringUntil('\n');
        command.trim();

        if (!command.isEmpty())
        {
            handleCommand(command);
        }
    }

private:
    void handleCommand(const String& command) 
    {
        float value;
        if (command.startsWith("setP ")) 
        {
            value = command.substring(5).toFloat();
            PIDController::getInstance().setP(value);
            Serial.println("P value set");
            return;
        } 
        
        if (command.startsWith("setI ")) 
        {
            value = command.substring(5).toFloat();
            PIDController::getInstance().setI(value);
            Serial.println("I value set");
            return;
        } 

        if (command.startsWith("setD ")) 
        {
            value = command.substring(5).toFloat();
            PIDController::getInstance().setD(value);
            Serial.println("D value set");
            return;
        } 
       
        if (command == "savePID") 
        {
            savePIDSettings();
            Serial.println("PID values saved to EEPROM");
            return;
        } 

        if (command == "loadPID") 
        {
            loadPIDSettings();
            Serial.println("PID values loaded from EEPROM");
            return;
        } 

        if (command == "printPID") 
        {
            float p, i, d;
            PIDController::getInstance().getPID(p, i, d);
            Serial.print("Current PID values - P: ");
            Serial.print(p, 4);
            Serial.print(", I: ");
            Serial.print(i, 5);
            Serial.print(", D: ");
            Serial.println(d, 4);
            return;
        } 
        
        if (command.startsWith("setTemp ")) 
        {
            value = command.substring(8).toFloat();
            PIDController::getInstance().setTarget(value);
            Serial.println("Temperature set");
            return;
        } 
        
        Serial.println("Invalid command: "+command);
    }
};
