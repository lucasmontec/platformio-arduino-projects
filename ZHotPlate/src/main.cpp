#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#include "SSD1306Wire.h"
#include "ThermistorSensor.h"

#include "PID.h"
#include "PIDPersistency.h"
#include "SerialPIDControl.h"

#include <ESP32RotaryEncoder.h>

#define DISPLAY_UPDATE_INTERVAL 150
#define PID_UPDATE_INTERVAL 40

//on a 70w-100w heating capsule, dont go over 400.
#define MAX_TEMP_C 330
#define MAX_PWM 255

// WIRING:

//I2C - 5 SDA, 6 SCL
//Any thin SSD1306 oled i2c connected

//ADS1115 analog to digital converter i2c connected

// Heating capsule at heatPin
// MOSFET, looking at the front of the chip (the side with no metal plate): left to right - gate - drain - source
// Using a 24v 50w capsule
// heatPin -- 1k ohm --> IRLB3034 (MOSFET) GATE
// IRLB3034 (MOSFET) GATE -- 10k ohm --> common ground (must be common between esp and power supply!)
// IRLB3034 (MOSFET) DRAIN --> heating capsule (-)
// IRLB3034 (MOSFET) SOURCE --> common ground, power supply -
// heating capsule (+) --> power supply +

uint8_t encoder_steps_per_rev = 4;
RotaryEncoder rotaryEncoder = RotaryEncoder(21, 20, 3, -1, encoder_steps_per_rev);

//Indicator addr led
// data - 8
Adafruit_NeoPixel indicator_led(1, 8, NEO_GRB + NEO_KHZ800);

//Push button - pin 0
int push_button_pin = 0;

//TUNING for just capsule and sensor: P 10, I 0.0008, D 55

ThermistorSensor thermistor(6, 5);  // Configure with 6 samples and a 5ms interval between samples
SSD1306Wire display(0x3c, 0, 1, GEOMETRY_128_32); 

PIDController& pidController = PIDController::getInstance();
SerialPIDCommandHandler& serialHandler =  SerialPIDCommandHandler::getInstance();

//Fault detection: temperature not increasing after a time
ulong heating_started_millis;
int start_temp;
int not_heating_fault_time = 15000;
//Fault detection: temperature not increasing after a time

int heater_pwm;
int heater_pin = 1;
String heating_status;
int targetTempToSet;

unsigned long nextDisplayUpdate;
unsigned long nextPIDUpdate;

bool heating;
float current_temperature_c;

const int flashing_freq_ms = 500;

uint32_t overrideColor;
ulong overrideColorUntil;

bool temperatureSensorFaulted;

ulong lastTempAdjust;
const int recentTempAjustTime = 1000;

void ShowColorIndication(uint32_t color, int duration)
{
  overrideColor = color;
  overrideColorUntil = millis() + duration;
}

void RotaryRotationCallback(long value)
{
	Serial.printf( "Value: %ld\n", value );
  targetTempToSet = value;
  lastTempAdjust = millis();
}

void StopHeating()
{
  start_temp = -1;
  heating = false;
  heating_started_millis = -1;
  heater_pwm = 0;
  PIDController::getInstance().setTarget(0);
  analogWrite(heater_pin, 0);
}

void StartHeatingToTargetTemp()
{
  start_temp = current_temperature_c;
  heating_started_millis = millis();
  PIDController::getInstance().setTarget(targetTempToSet);
  heating = targetTempToSet > 0;
}

void CheckTempSensorFault()
{
  ulong heatingTime = millis() - heating_started_millis;
  float target = PIDController::getInstance().getTarget();
  
  if(
    heating &&
    heatingTime > not_heating_fault_time &&
    target > 30 &&
    current_temperature_c - start_temp < 10
    )
  {
    StopHeating();
    temperatureSensorFaulted = true;
  }

  if(!temperatureSensorFaulted)
  {
    temperatureSensorFaulted = thermistor.hasError();
  }
}

void RotaryButtonCallback(unsigned long duration)
{
  if(temperatureSensorFaulted)
  {
    return;
  }

  if(duration > 800)
  {
    ShowColorIndication(indicator_led.Color(0, 0, 255), 500);
    targetTempToSet = 0;
  }
  else
  {
    ShowColorIndication(indicator_led.Color(60, 60, 180), 250);
  }

  StartHeatingToTargetTemp();
}

void HandlePushButton()
{
  bool buttonPressed = !digitalRead(push_button_pin);
  if(buttonPressed)
  {
    StopHeating();
  }
}

void UpdateIndicatorLed()
{
  uint32_t color = indicator_led.Color(0, 255, 0);

  if(heating)
  {
    color = indicator_led.Color(255, 255, 0);
  }

  if(current_temperature_c > 45) //pain temp
  {
    int redValue = ((millis() / flashing_freq_ms) % 2) * 255;
    color = indicator_led.Color(redValue, 255, 0);
  }

  if(overrideColorUntil > millis())
  {
    color = overrideColor;
  }

  indicator_led.setPixelColor(0, color);
  indicator_led.show();
}

void SerialPrintTempK()
{
  Serial.print("Temperature: ");
  Serial.print(current_temperature_c);
  Serial.println("C");
}

void UpdateDisplay()
{
  display.clear();

  //Border
  display.drawRect(0,0, 128, 32);

  display.setFont(ArialMT_Plain_16);

  if(temperatureSensorFaulted)
  {
    display.drawString(2, 1, "FAULT. TEMP SENSOR");
    display.display();
    return;
  }
  
  //Temp display  
  String tempString = String(current_temperature_c, 1)+"C T:";
  display.drawString(2, 1, tempString);
  
  String displayTemp = "";
  uint16_t tempWidth = display.getStringWidth(tempString);//max size string width

  if(heating)
  {
    bool movedTempRecently = millis() - lastTempAdjust < recentTempAjustTime;

    display.setFont(movedTempRecently ? ArialMT_Plain_10 : ArialMT_Plain_16);

    displayTemp = String((int)pidController.getTarget()) + "/";
    display.drawString(2+tempWidth, 1, displayTemp);
    int smallWidth = display.getStringWidth(displayTemp);
    display.setFont(movedTempRecently ? ArialMT_Plain_16 : ArialMT_Plain_10);
    display.drawString(2+tempWidth+smallWidth, 1, String((int)targetTempToSet));
  }
  else
  {
    display.setFont(ArialMT_Plain_16);
    displayTemp = String((int)targetTempToSet);
    display.drawString(2+tempWidth, 1, displayTemp);
  }

  display.setFont(ArialMT_Plain_10);
  display.drawString(2, 17, "status: "+heating_status);

  //PWM indicator
  int barH = 31;
  int pwmBarH = heating ? ((float)heater_pwm/255.0f) * (float)barH : barH;
  display.fillRect(128-5, 1, 5, pwmBarH);

  display.display();
}

void UpdatePIDHeater()
{
  if(!heating)
  {
    heater_pwm = 0;
    return;
  }

  if(current_temperature_c <= 0 || millis() <= nextPIDUpdate)
  {
    return;
  }

  nextPIDUpdate = millis() + PID_UPDATE_INTERVAL;

  heater_pwm = pidController.calculatePID(current_temperature_c);
  
  analogWrite(heater_pin, heater_pwm);
}

void UpdateHeatingStatus()
{
  if (!heating)
  {
    heating_status = "OFF";
    return;
  }

  if (heater_pwm == 0)
  {
    heating_status = "IDLE";
    return;
  }

  if (heater_pwm < 50)
  {
    heating_status = "KEEPING";
    return;
  }
  
  heating_status = "HEATING";
}

void setup()
{
  Serial.begin(115200);

	rotaryEncoder.setBoundaries( 0, MAX_TEMP_C, true );
  rotaryEncoder.onTurned( &RotaryRotationCallback );
  rotaryEncoder.onPressed( &RotaryButtonCallback );
  rotaryEncoder.begin();

  indicator_led.begin();
  indicator_led.setPixelColor(0, indicator_led.Color(0, 150, 0));
  indicator_led.setBrightness(50);
  indicator_led.show(); 

  pinMode(push_button_pin, INPUT_PULLUP);

  pinMode(heater_pin, OUTPUT); 
  analogWrite(heater_pin, 0);

  Wire.begin(5, 6);

  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);

  bool success = thermistor.begin();
  if(!success)
  {
    temperatureSensorFaulted = true;
  }

  pidController.begin();
  pidController.setMaxPWMOut(MAX_PWM);
  
  loadPIDSettings();

  Serial.println("Ready!");
}

void loop() 
{
  thermistor.update();
  serialHandler.processCommands();

  current_temperature_c = thermistor.temperatureCelsius();

  if (current_temperature_c > 0 && millis() > nextDisplayUpdate) // Check if a valid temperature average is ready
  {  
    nextDisplayUpdate = millis() + DISPLAY_UPDATE_INTERVAL;
    UpdateDisplay();
  }

  UpdatePIDHeater();
  UpdateIndicatorLed();
  HandlePushButton();
  UpdateHeatingStatus();
}