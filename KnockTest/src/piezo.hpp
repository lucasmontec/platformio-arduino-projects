#pragma once
#include <Arduino.h>

class Piezo
{
  private:
    unsigned int _maxSamples = 10;
    int _pin;
    int _minDetectionInterval;
    void (*_onHit)(float);
    
    int* samples;
    byte sampleCount = 0;
    int currentPinRead;
    
    unsigned int currentSampleIndex = 0;
    unsigned long nextDetection = 0;

    bool hit;

    void readSample()
    {
        currentPinRead = analogRead(_pin);
        /*
        int rawRead = 0;
        currentPinRead = -1;
        for(int i=0; i<250; i++)
        {
            rawRead = analogRead(_pin);
            if(rawRead > currentPinRead){
                currentPinRead = rawRead;
            }
        }*/

        samples[currentSampleIndex % _maxSamples] = currentPinRead;
        currentSampleIndex++;

        if(sampleCount < _maxSamples){
            sampleCount++;
        }
    }

    unsigned long calculateAverageSample()
    {
        unsigned long currentAverage = 0;
        for(unsigned int i = 0; i < sampleCount; i++)
        {
            currentAverage += samples[i];
        }
        //Serial.println("total sum: "+String(currentAverage));
        currentAverage /= sampleCount;
        //Serial.println("currentAverage: "+String(currentAverage)+" sample count "+String(sampleCount));

        return currentAverage;
    }

    void checkHit()
    {
        hit = false;
        int avgSample = calculateAverageSample();

        int increasedSampleAvg = avgSample*1.02f;

        unsigned long now = millis();

        if (currentPinRead >= increasedSampleAvg && currentPinRead > 0 && increasedSampleAvg > 0) {
            if(now > nextDetection)
            {
                delayDetection();
                
                if(_onHit != NULL) {
                    _onHit(currentPinRead);
                }
                hit = true;
                //Serial.println("Hit! "+String(currentPinRead)+" "+String(increasedSampleAvg)+" now "+String(now)+" nextDetection "+String(nextDetection));
            }
        }
    }

  public:
    Piezo(int pin, int maxSamples, int minDetectionInterval, void (*onHit)(float))
    {
        _maxSamples = maxSamples;
        samples = new int[maxSamples];
        _pin = pin;
        _onHit = onHit;
        _minDetectionInterval = minDetectionInterval;

        hit = false;

        pinMode(pin, INPUT_PULLUP);
        //pinMode(pin, INPUT);
    }

    void delayDetection()
    {
        unsigned long now = millis();
        nextDetection = now + _minDetectionInterval;
    }

    void update() 
    {
        readSample();
        checkHit();
    }

    bool hasHit()
    {
        return hit;
    }

    float currentRead()
    {
        return ((float)currentPinRead) / calculateAverageSample();
    }

    int raw()
    {
        return currentPinRead;
    }

    unsigned long rawAvg()
    {
        return calculateAverageSample();
    }

    int rawSampleCount()
    {
        return sampleCount;
    }
};