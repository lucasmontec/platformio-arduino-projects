#include <Arduino.h>
#include "piezo.hpp"

Piezo *piezo1;
Piezo *piezo2;

void flash(int pin);

void setup() {
  //Serial.begin(9600);
  //Serial.println("piezo1 piezo2");

/*
  piezo1 = new Piezo(A0, 100, 2000, NULL);
  piezo2 = new Piezo(A1, 100, 2000, NULL);
  */

  piezo1 = new Piezo(A0, 40, 2000, NULL);
  piezo2 = new Piezo(A1, 40, 2000, NULL);

  pinMode(4, OUTPUT);
  pinMode(12, OUTPUT);

  flash(4);
  flash(12);
}

void loop() {
  piezo1->update();
  piezo2->update();

  //Serial.println(String(piezo1->raw())+" "+String(piezo2->raw()));
  //Serial.println(String(piezo1->currentRead()));
  if(piezo1->hasHit() && !piezo2->hasHit())
  {
    //Serial.println("Hit 1: "+String(piezo1->currentRead())+" raw read "+String(piezo1->raw())+" sample count "+String(piezo1->rawSampleCount())+" average "+String(piezo1->rawAvg()));
    //Serial.println("===============");
    flash(4);
    piezo2->delayDetection();
    return;
  }

  if(!piezo1->hasHit() && piezo2->hasHit())
  {
    //Serial.println("Hit 2: "+String(piezo1->currentRead()));
    //Serial.println("===============");
    flash(12);
    piezo1->delayDetection();
    return;
  }

  if(piezo1->hasHit() && piezo2->hasHit())
  {
    if(piezo1->currentRead() > piezo2->currentRead())
    {
      //Serial.println("Hitboth 1: "+String(piezo1->currentRead()));
      //Serial.println("===============");
      flash(4);
      return;
    }
    
    //Serial.println("Hitboth 2: "+String(piezo1->currentRead()));
    //Serial.println("===============");
    flash(12);
    return;
  }
}

void flash(int pin)
{
  digitalWrite(pin, HIGH);
  delay(50);
  digitalWrite(pin, LOW); 
  delay(50);  
  digitalWrite(pin, HIGH);
  delay(50);
  digitalWrite(pin, LOW); 
  delay(50);  
}